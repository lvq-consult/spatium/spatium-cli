# Spatium CLI

This is a reeaaally simple cli for managing timesheets in flat json files, preferably in a git repo (this will become a requirement at some point).

Using this cli in a git repo will give you a sort of ledger, which is really nice.

## Installation

It is recommended to create a new Git repo, where you will store the timesheets.

So remember to `npm init` and then just `npm i --save spatium-cli`.

We recommend adding a npm script:

```json
{
  "sp": "spatium-cli"
}
```
