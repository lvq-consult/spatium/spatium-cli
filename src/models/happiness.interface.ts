import { SpatiumEntity } from '@lvqconsult/spatium-db';

export interface Happiness extends SpatiumEntity {
  timesheetId: string;
  score: number;
  mood: string;
}
