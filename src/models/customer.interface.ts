import { SpatiumEntity } from '@lvqconsult/spatium-db';

export interface Customer extends SpatiumEntity {
  name: string;
  cvr: string;
}
