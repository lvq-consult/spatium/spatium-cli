import { SpatiumEntity } from '@lvqconsult/spatium-db';

export interface Assignment extends SpatiumEntity {
  customerId: string;
  agentId: string;
  userId: string;
}
