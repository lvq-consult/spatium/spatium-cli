import { SpatiumEntity } from '@lvqconsult/spatium-db';

export interface Agent extends SpatiumEntity {
  name: string;
  cvr: string;
}
