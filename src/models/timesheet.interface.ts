import { SpatiumEntity } from '@lvqconsult/spatium-db';

export interface Timesheet extends SpatiumEntity {
  customerId: string;
  userId: string;
  date: string;
  task: string;
  hours: number;
  minutes: number;
  note: string;
}
