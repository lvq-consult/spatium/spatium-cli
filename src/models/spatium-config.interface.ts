import { SpatiumEntity } from '@lvqconsult/spatium-db';

export interface SpatiumDropboxConfig {
  appKey: string;
  appSecret: string;
  accessToken:
    | string
    | {
        access_token: string;
        token_type: string;
        uid: string;
        account_id: string;
      };
}

export interface SpatiumTime {
  ianaZone: string;
  locale: string;
}

export interface SpatiumGitIntegration {
  enabled: boolean;
  name: string;
  email: string;
  publicKeyPath: string;
  privateKeyPath: string;
}

export interface SpatiumCvrApiIntegration {
  enabled: boolean;
  companyName: string;
  projectName: string;
  contactName: string;
  contactPhone?: string;
  contactEmail?: string;
  useEmail: boolean;
}

export interface SpatiumConfig {
  dropbox: SpatiumDropboxConfig;
  time: SpatiumTime;
  git: SpatiumGitIntegration;
  cvrApi: SpatiumCvrApiIntegration;
  lastSelection: {
    customerId: string;
    userId: string;
  };
}

export interface SpatiumConfigBaseEntity<TKey extends keyof SpatiumConfig, TValue extends SpatiumConfig[TKey]>
  extends SpatiumEntity {
  key: TKey;
  value: TValue;
}
