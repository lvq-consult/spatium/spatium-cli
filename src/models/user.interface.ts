import { SpatiumEntity } from '@lvqconsult/spatium-db';

export interface User extends SpatiumEntity {
  username: string;
  firstname: string;
  lastname: string;
}
