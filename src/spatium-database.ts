import { SpatiumDatabase as Database } from '@lvqconsult/spatium-db';
import { resolve } from 'path';
import { singleton } from 'tsyringe';
import { Agent } from './models/agent.interface';
import { Assignment } from './models/assignment.interface';
import { Customer } from './models/customer.interface';
import { Happiness } from './models/happiness.interface';
import { SpatiumConfigBaseEntity } from './models/spatium-config.interface';
import { Timesheet } from './models/timesheet.interface';
import { User } from './models/user.interface';

@singleton()
export class SpatiumDatabase extends Database {
  public agents = this.getTable<Agent>('agents', ['cvr', 'name']);
  public assignments = this.getTable<Assignment>('assignments', ['agentId', 'customerId', 'userId']);
  public config = this.getTable<SpatiumConfigBaseEntity<any, any>>('config', ['key']);
  public customers = this.getTable<Customer>('customers', ['cvr', 'name']);
  public happiness = this.getTable<Happiness>('happiness', ['timesheetId']);
  public timesheets = this.getTable<Timesheet>('timesheets', [
    'customerId',
    'date',
    'note',
    'sequence',
    'task',
    'userId',
  ]);
  public users = this.getTable<User>('users', ['firstname', 'lastname', 'username']);

  constructor() {
    super(resolve(process.cwd(), 'data', 'v2'));
  }
}
