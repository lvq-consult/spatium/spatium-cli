import { Timesheet } from '../models/timesheet.interface';

export function calculateAggregate(timesheets: Timesheet[]) {
  const aggregate = timesheets.reduce((sum, current) => {
    if (sum[current.task] === undefined) {
      sum[current.task] = {
        hours: 0,
        minutes: 0,
      };
    }

    sum[current.task].hours += current.hours;
    sum[current.task].minutes += current.minutes;

    if (sum[current.task].minutes >= 60) {
      const hoursFromMinutes = Number(Math.floor(sum[current.task].minutes / 60));
      const m = sum[current.task].minutes - hoursFromMinutes * 60;

      sum[current.task].hours += hoursFromMinutes;
      sum[current.task].minutes = m;
    }

    return sum;
  }, {} as { [task: string]: { hours: number; minutes: number } });

  return aggregate;
}
