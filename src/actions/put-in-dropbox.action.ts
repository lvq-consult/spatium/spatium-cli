import { Dropbox } from 'dropbox';
import { exists, readFile } from 'fs-extra';
import * as fetch from 'isomorphic-fetch';
import { trimStart } from 'lodash';
import { basename } from 'path';
import { injectable } from 'tsyringe';
import { error, info } from '../logger';
import { ConfigRepository } from '../repositories/config.repository';

@injectable()
export class PutInDropboxAction {
  constructor(public config: ConfigRepository) {}

  public async execute(dropboxFolder: string, filepath: string) {
    this.log('executing');
    const filename = basename(filepath);
    const folder = `/${trimStart(dropboxFolder, '/')}`;

    try {
      const fileDoesExists = await this.exists(filepath);

      if (!fileDoesExists) {
        error('unable to find file', filepath);

        return;
      }

      const { accessToken: configAccessToken } = await this.config.dropbox;
      const accessToken = typeof configAccessToken === 'string' ? configAccessToken : configAccessToken?.access_token;

      if (!accessToken) {
        error('missing access token');

        return;
      }

      const contents = await readFile(filepath);
      const dbx = new Dropbox({ accessToken, fetch });
      const folderExists = await this.folderExistsInDropbox(dbx, folder);

      if (!folderExists) {
        info('creating', folder, 'folder in dropbox');
        await dbx.filesCreateFolderV2({ path: folder });
      } else {
        info(folder, 'already exists in dropbox');
      }

      const fileExists = await this.fileExistsInDropbox(dbx, folder, filename);
      const path = `${folder}/${filename}`;

      if (fileExists) {
        this.log(`${path} already exists in dropbox, deleting it`);
        info(path, 'already exists in dropbox, deleting it');
        await dbx.filesDeleteV2({ path });
      }

      info('uploading to', path);
      this.log(`uploading to: ${path}`);
      await dbx.filesUpload({ path: folder, contents });
    } catch (e) {
      console.error(e);

      process.exit(-1);
    }
  }

  private async folderExistsInDropbox(dbx: Dropbox, folder: string) {
    return this.thingExistsInDropbox(dbx, '', folder, 'folder');
  }

  private async fileExistsInDropbox(dbx: Dropbox, path: string, filename: string) {
    return this.thingExistsInDropbox(dbx, path, filename, 'file');
  }

  private async thingExistsInDropbox(dbx: Dropbox, path: string, thing: string, tag?: 'folder' | 'file') {
    try {
      let current = await this.dropboxFileListFolder(dbx, path);

      do {
        try {
          const result = current.entries.findIndex(v => {
            const nameMatch = v.name === thing;
            const tagMatch = tag ? v['.tag'] === tag : true;

            return nameMatch && tagMatch;
          });

          if (result !== -1) {
            return true;
          }

          current = await this.dropboxFileListFolderContinue(dbx, current.cursor);
        } catch (e) {
          console.error(e);

          return false;
        }
      } while (current.has_more !== false);

      return false;
    } catch (e) {
      console.error(e);

      return false;
    }
  }

  private async exists(path: string) {
    return new Promise(resolve => exists(path, itIsThere => resolve(itIsThere)));
  }

  private log(message: string) {
    console.log(`> [put-in-dropbox-action] ${message}`);
  }

  private async dropboxFileListFolder(dbx: Dropbox, path: string) {
    return dbx.filesListFolder({ path }).then(response => ({
      entries: response.result.entries,
      has_more: response.result.has_more,
      cursor: response.result.cursor,
    }));
  }

  private async dropboxFileListFolderContinue(dbx: Dropbox, cursor: string) {
    return dbx.filesListFolderContinue({ cursor }).then(response => ({
      entries: response.result.entries,
      has_more: response.result.has_more,
      cursor: response.result.cursor,
    }));
  }
}
