import { Column, Workbook } from 'exceljs';
import { exists, mkdirp, writeJson } from 'fs-extra';
import { resolve } from 'path';
import { injectable } from 'tsyringe';
import { info } from '../logger';
import { Customer } from '../models/customer.interface';
import { Timesheet } from '../models/timesheet.interface';
import { CustomerRepository } from '../repositories/customer.repository';
import { TimesheetRepository } from '../repositories/timesheet.repository';
import { UserRepository } from '../repositories/user.repository';
import { DateHelperService } from '../services/date-helper.service';
import { calculateAggregate } from './calculate-aggregate.fn';
import { ExportHtmlAction } from './export-html.actions';
import { RenderPdfAction } from './render-pdf.action';

export type ExportFormat = 'csv' | 'json' | 'pdf' | 'html' | 'xlsx';
export type ExportFormats = ['csv', 'json', 'pdf', 'html', 'xlsx'] | ExportFormat[];
export const ExportFormatValues: ExportFormats = ['csv', 'json', 'pdf', 'html', 'xlsx'];

@injectable()
export class ExportAction {
  constructor(
    public customers: CustomerRepository,
    public timesheets: TimesheetRepository,
    public users: UserRepository,
    public dateHelper: DateHelperService,
    public renderPdfAction: RenderPdfAction,
    public exportHtmlAction: ExportHtmlAction
  ) {}

  public async execute(
    format: ExportFormat,
    customerId: string,
    start: string,
    end: string,
    directory: string,
    nosandbox: boolean
  ) {
    try {
      this.log('executing');
      const startDate = await this.dateHelper.fromISO(start);
      const endDate = await this.dateHelper.fromISO(end);
      const customer = await this.customers.findOne(customerId);

      if (directory !== null) {
        this.log(`${directory} not found`);
        await mkdirp(resolve(process.cwd(), directory));
      }

      const filePath = await this.getFilePath(directory, start, customer, format);
      const customerTimesheets = await this.timesheets.where('customerId', customerId);
      const entries = customerTimesheets
        .filter(timesheet => {
          const date = this.dateHelper.fromISOSync(timesheet.date);

          return date >= startDate && date <= endDate;
        })
        .sort((a, b) => (a?.date ?? '').localeCompare(b?.date ?? ''));
      this.log(`${entries.length} entries found`);

      if (entries.length === 0) {
        this.log(`no entries found not exporting: ${customer.name}`);

        return;
      }

      switch (format) {
        case 'csv':
          this.log('rendering csv');
          await this.renderCsv(filePath, entries);
          break;
        case 'xlsx':
          this.log('rendering xlsx');
          await this.renderXlsx(filePath, entries);
          break;
        case 'json':
          this.log('rendering json');
          await this.renderJson(filePath, entries);
          break;
        case 'pdf':
          this.log('rendering pdf');
          // eslint-disable-next-line no-case-declarations
          const html = await this.renderHtml(customer, filePath, entries, true);

          await this.renderPdfAction
            .withOptions({
              launch: {
                args: nosandbox ? ['--no-sandbox', '--disable-setuid-sandbox'] : [],
              },
              pdf: {
                format: 'a4',
                displayHeaderFooter: true,
                margin: {
                  bottom: '3cm',
                  left: '1cm',
                  right: '1cm',
                  top: '1cm',
                },
                headerTemplate: '',
                // tslint:disable-next-line:max-line-length
                footerTemplate: `<div style="display: block; text-align: center; font-size: 6pt; margin: 0 auto; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',Arial,'Noto Sans',sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol','Noto Color Emoji';">
  <div style="display: block;">Generated with Spatium CLI</div>
  <div style="display: block;"><strong>Page <span class="pageNumber"></span> of <span class="totalPages"></span></strong></div>
</div>`,
              },
            })
            .renderFromHtmlToFile(html, filePath);
          break;
        case 'html':
          this.log('rendering html');
          await this.renderHtml(customer, filePath, entries);
          break;
        default:
          throw new Error(`unknown export format: ${format}`);
      }

      this.log(`rendered: ${filePath}`);

      return filePath;
    } catch (e) {
      console.error(e);
    }
  }

  public async getFilenamePrefix(start: string) {
    const startDate = await this.dateHelper.fromISO(start);
    const month = startDate.month.toString();
    const year = startDate.year;

    return `${year}-${month.length === 1 ? `0${month}` : month}`;
  }

  private async renderJson(filePath: string, timesheets: Timesheet[]) {
    const { hours, minutes } = this.getTotals(timesheets);
    const aggregate = calculateAggregate(timesheets);

    await writeJson(
      filePath,
      {
        generatedBy: 'Spatium CLI',
        hours,
        minutes,
        aggregate,
        timesheets,
      },
      { spaces: 2 }
    );
  }

  private async renderHtml(customer: Customer, filePath: string, timesheets: Timesheet[], toPrint = false) {
    return await this.exportHtmlAction.execute(this.getTotals(timesheets), customer, filePath, timesheets, toPrint);
  }

  private async renderCsv(filePath: string, timesheets: Timesheet[]) {
    const workbook = await this.createWorkbook(timesheets);

    await workbook.csv.writeFile(filePath);
  }

  private async renderXlsx(filePath: string, timesheets: Timesheet[]) {
    const workbook = await this.createWorkbook(timesheets);

    await workbook.xlsx.writeFile(filePath);
  }

  private async getFilePath(directory: string, start: string, customer: Customer, extension: ExportFormat) {
    const dir = directory !== null ? directory : '';
    const initialFilename = await this.getFilename(start, customer, extension);
    const initialFilePath = resolve(process.cwd(), dir, initialFilename);
    const initialFilePathExists = await this.fileExists(initialFilePath);
    info(initialFilename, 'exists?:', initialFilePathExists);
    if (!initialFilePathExists) {
      return initialFilePath;
    }

    for (let pass = 1; pass < 255; pass++) {
      const fileName = await this.getFilename(start, customer, extension, pass.toString());
      const filePath = resolve(process.cwd(), dir, fileName);
      const filePathExists = await this.fileExists(filePath);
      info(fileName, 'exists?:', filePathExists);

      if (!filePathExists) {
        return filePath;
      }
    }

    throw new Error('File already exists');
  }

  private async getFilename(start: string, customer: Customer, extension: ExportFormat, suffix?: string) {
    const prefix = await this.getFilenamePrefix(start);
    const actualSuffix = suffix ? `.${suffix}` : '';
    const scrubbedCustomerName = (customer?.name || 'MISSING CUSTOMER NAME').replace(new RegExp(/\/\\/, 'g'), '');

    return `${prefix} ${scrubbedCustomerName}${actualSuffix}.${extension}`;
  }

  private async createWorkbook(timesheets: Timesheet[]) {
    const workbook = new Workbook();

    workbook.creator = 'LVQ Consult ApS';
    workbook.created = new Date();

    const worksheet = workbook.addWorksheet('Timesheet');

    worksheet.columns = [
      { header: 'Dato', key: 'date' } as unknown as Column,
      { header: 'Opgave', key: 'task' } as unknown as Column,
      { header: 'Timer', key: 'hours' } as unknown as Column,
      { header: 'Minutter', key: 'minutes' } as unknown as Column,
      { header: 'Note', key: 'note' } as unknown as Column,
    ];

    const rows = await Promise.all(
      timesheets.map(async timesheet => ({
        ...timesheet,
        date: await this.dateHelper.formatDate(timesheet.date),
      }))
    );

    worksheet.addRows(rows);

    const { hours, minutes } = this.getTotals(timesheets);

    worksheet.addRow({
      date: 'Total',
      note: '',
      task: '',
      hours,
      minutes,
    } as Partial<Timesheet>);

    const aggreate = calculateAggregate(timesheets);

    worksheet.addRow({
      date: '',
      note: '',
      task: '',
      hours: '',
      minutes: '',
    });

    Object.keys(aggreate)
      .sort()
      .forEach(task =>
        worksheet.addRow({
          date: '',
          note: '',
          task: task,
          hours: aggreate[task].hours,
          minutes: aggreate[task].minutes,
        })
      );

    worksheet.addRows([{ date: '' }, { date: 'Generated by Spatium CLI' }]);

    return workbook;
  }

  private getTotals(timesheets: Timesheet[]) {
    const totalHours = timesheets.reduce((sum, current) => (sum += current.hours), 0);
    const totalMinutes = timesheets.reduce((sum, current) => (sum += current.minutes), 0);
    const hoursFromMinutes = Number(Math.floor(totalMinutes / 60));
    const hours = totalHours + hoursFromMinutes;
    const minutes = totalMinutes - hoursFromMinutes * 60;

    return { hours, minutes };
  }

  private async fileExists(path: string) {
    return new Promise(accept => exists(path, itIsThere => accept(itIsThere)));
  }

  private log(message: string) {
    console.log(`> [export-action] ${message}`);
  }
}
