import { Dropbox } from 'dropbox';
import { exists, readFile } from 'fs-extra';
import * as fetch from 'isomorphic-fetch';
import { trimEnd, trimStart } from 'lodash';
import { basename } from 'path';
import { injectable } from 'tsyringe';
import { error, info } from '../logger';
import { ConfigRepository } from '../repositories/config.repository';

@injectable()
export class PutManyInDropboxAction {
  constructor(public config: ConfigRepository) {}

  public async execute(dropboxFolder: string, filepaths: string[]) {
    this.log('executing');

    if (!filepaths?.length) {
      this.log('nothing to put in dropbox');
      return;
    }

    const folder = `/${trimStart(dropboxFolder, '/')}`;

    try {
      for (const filepath of filepaths) {
        const fileDoesExists = await this.exists(filepath);

        if (!fileDoesExists) {
          error('unable to find file', filepath);

          filepaths.splice(
            filepaths.findIndex(f => f === filepath),
            1
          );
        }
      }

      const { accessToken: configAccessToken } = await this.config.dropbox;
      const accessToken = typeof configAccessToken === 'string' ? configAccessToken : configAccessToken?.access_token;

      if (!accessToken) {
        error('missing access token');

        return;
      }

      const contents$ = await Promise.all(
        filepaths.map(async filepath => {
          const content = await readFile(filepath);

          return { [filepath]: content };
        })
      );
      const contentsMap = contents$.reduce((sum, curr) => ({ ...sum, ...curr }), {} as { [key: string]: Buffer });
      const dbx = new Dropbox({ accessToken, fetch });
      const folderExists = await this.folderExistsInDropbox(dbx, folder);

      if (!folderExists) {
        this.verboseLog(`creating ${folder} folder in dropbox`);
        await dbx.filesCreateFolderV2({ path: folder });
      } else {
        this.verboseLog(`${folder} already exists in dropbox`);
      }

      for (const filepath of filepaths) {
        const filename = basename(filepath);
        const fileExists = await this.fileExistsInDropbox(dbx, folder, filename);
        const path = `${folder}/${filename}`;
        const mode: any = { '.tag': fileExists ? 'overwrite' : 'add' };

        try {
          const contents = contentsMap[filepath];
          this.verboseLog(`${trimEnd(mode['.tag'], 'e')}ing ${path}`);
          const start = new Date().getTime();
          const uploadResponse = await dbx.filesUpload({ path, contents, mode });
          const meta = uploadResponse.result;
          const end = new Date().getTime();
          const time = end - start;
          const seconds = time / 1000;
          const bytes = contents.byteLength;
          const kiloBytes = bytes / 1024;
          const speed = (kiloBytes / seconds).toFixed(2);

          this.verboseLog(`uploaded ${meta.path_display} (${kiloBytes} KB) in ${time}ms @ ${speed} KB/s`);
          this.log(`uploaded ${meta.path_display} (${kiloBytes} KB) in ${time}ms @ ${speed} KB/s`);
        } catch (e) {
          console.error(e);
          process.exit(-1);
        }
      }
    } catch (e) {
      console.error(e);
      process.exit(-1);
    }
  }

  private async folderExistsInDropbox(dbx: Dropbox, folder: string) {
    return this.thingExistsInDropbox(dbx, '', trimStart(folder, '/'), 'folder');
  }

  private async fileExistsInDropbox(dbx: Dropbox, path: string, filename: string) {
    return this.thingExistsInDropbox(dbx, path, filename, 'file');
  }

  private async thingExistsInDropbox(dbx: Dropbox, path: string, thing: string, tag?: 'folder' | 'file') {
    try {
      let current = await this.dropboxFileListFolder(dbx, path);

      do {
        try {
          const result = current.entries.findIndex(v => {
            const nameMatch = v.name === thing;
            const tagMatch = tag ? v['.tag'] === tag : true;

            return nameMatch && tagMatch;
          });

          if (result !== -1) {
            return true;
          }

          current = await this.dropboxFileListFolderContinue(dbx, current.cursor);
        } catch (e) {
          error(e);

          return false;
        }
      } while (current.has_more !== false);

      return false;
    } catch (e) {
      error(e);

      return false;
    }
  }

  private async exists(path: string) {
    return new Promise(resolve => exists(path, itIsThere => resolve(itIsThere)));
  }

  private log(message: string) {
    console.log(`> [put-many-in-dropbox-action] ${message}`);
  }

  private verboseLog(message: string) {
    info(`[put-many-in-dropbox-action] ${message}`);
  }

  private async dropboxFileListFolder(dbx: Dropbox, path: string) {
    return dbx.filesListFolder({ path }).then(response => ({
      entries: response.result.entries,
      has_more: response.result.has_more,
      cursor: response.result.cursor,
    }));
  }

  private async dropboxFileListFolderContinue(dbx: Dropbox, cursor: string) {
    return dbx.filesListFolderContinue({ cursor }).then(response => ({
      entries: response.result.entries,
      has_more: response.result.has_more,
      cursor: response.result.cursor,
    }));
  }
}
