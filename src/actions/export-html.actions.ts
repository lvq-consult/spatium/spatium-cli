import { writeFile } from 'fs-extra';
import { basename } from 'path';
import { format as formatCode } from 'prettier';
import { injectable } from 'tsyringe';
import { distinctFilter } from '../distinct-filter.fn';
import { Customer } from '../models/customer.interface';
import { Timesheet } from '../models/timesheet.interface';
import { UserRepository } from '../repositories/user.repository';
import { DateHelperService } from '../services/date-helper.service';
import { calculateAggregate } from './calculate-aggregate.fn';
import { createTable } from './html/table-factory.fn';

@injectable()
export class ExportHtmlAction {
  constructor(private dateHelper: DateHelperService, private users: UserRepository) {}

  public async execute(
    totals: {
      hours: number;
      minutes: number;
    },
    customer: Customer,
    filePath: string,
    timesheets: Timesheet[],
    toPrint = false
  ) {
    try {
      this.log('executing');
      const header = ['Date', 'Task', 'Hours', 'Minutes', 'Note'];

      const usersInTimesheets = (
        await Promise.all(
          timesheets
            .map(({ userId }) => userId)
            .filter(userId => userId !== undefined)
            .filter(distinctFilter)
            .map(userId => this.users.findOne(userId))
        )
      ).filter(user => !!user);
      const rows = await Promise.all(
        timesheets.map(async ({ date, task, hours, minutes, note }) => [
          await this.dateHelper.formatDate(date),
          task ?? '',
          hours ?? 0,
          minutes ?? 0,
          note ?? '',
        ])
      );
      const footer = ['Total', '', totals.hours, totals.minutes, ''];
      const aggregateHeader = ['Task', 'Hours', 'Minutes'];
      const aggregateFooter = ['Total', totals.hours, totals.minutes];
      const aggregateData = calculateAggregate(timesheets);
      const aggregate = Object.keys(aggregateData).map(key => [
        key,
        aggregateData[key].hours,
        aggregateData[key].minutes,
      ]);
      const table = createTable({ header, rows, footer });
      const aggregateTable = createTable({ header: aggregateHeader, rows: aggregate, footer: aggregateFooter });
      const body = `
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>${basename(filePath)}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <style>
      body {
        padding-top: 1rem;
      }
    </style>
  </head>
  <body ${toPrint ? 'class="print"' : ''}>
    <div class="container">
      <div class="row">
        <div class="col">
          <h1>${customer.name}</h1>
          <p class="text-muted">CVR: ${customer.cvr}</p>
        </div>
      </div>
      ${
        usersInTimesheets.length === 0
          ? ''
          : `
      <div class="row">
        <div class="col">
          <h2>Consultants</h2>
          <ul>
            ${usersInTimesheets.map(u => `<li class="text-muted">${u.firstname} ${u.lastname} (${u.username})</li>`)}
          </ul>
        </div>
      </div>
      `
      }
      <div class="row">
        <div class="col">
          <h2>Timesheets</h2>
        </div>
      </div>
      <div class="row">
        <div class="col">
          ${table.render()}
        </div>
      </div>
      <div class="row">
        <div class="col">
          <h2>Aggregate</h2>
        </div>
      </div>
      <div class="row">
        <div class="col">
          ${aggregateTable.render()}
        </div>
      </div>
    </div>
  </body>
</html>
`;

      const formatedBody = formatCode(body, {
        parser: 'html',
        tabWidth: 2,
        useTabs: false,
      });

      await writeFile(filePath, formatedBody, { encoding: 'utf-8' });

      return body;
    } catch (e) {
      console.error(e);
      process.exit(-1);
    }
  }

  private log(message: string) {
    console.log(`> [export-html-action] ${message}`);
  }
}
