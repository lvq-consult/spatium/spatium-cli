import { StrongElement } from './elements/strong.element';
import { TableBodyElement } from './elements/table-body.element';
import { TableCellElement } from './elements/table-cell.element';
import { TableHeadElement } from './elements/table-head.element';
import { TableHeaderCellElement } from './elements/table-header-cell.element';
import { TableRowElement } from './elements/table-row.element';
import { TableElement } from './elements/table.element';
import { TableFactoryOptions } from './interfaces/table-factory-options.interface';

interface HtmlCellOptions {
  bold?: boolean;
  isHeader?: boolean;
  scope?: 'col' | 'row';
}

class HtmlCell {
  constructor(public cell: string | number, public options: HtmlCellOptions = HtmlCell.defaultOptions) {}

  public static defaultOptions: HtmlCellOptions = { bold: false, isHeader: false };

  public render() {
    const cell = this.options.isHeader ? new TableHeaderCellElement() : new TableCellElement();
    const text = this.cell.toString();

    if (this.options.bold) {
      cell.addChild(new StrongElement(text));
    } else {
      cell.textContent = text;
    }

    if (typeof this.cell === 'number') {
      cell.addClass('text-right');
    }

    if (this.options.scope) {
      cell.setScope(this.options.scope);
    }

    return cell;
  }
}

class HtmlRow {
  constructor(public cells: HtmlCell[]) {}

  public render() {
    const row = new TableRowElement();
    const cells = this.cells.map(cell => cell.render());

    row.addCell(...cells);

    return row;
  }
}

class HtmlTableRenderer {
  private _header: HtmlRow;
  private _rows: HtmlRow[] = [];

  public withHeader(headers: string[]) {
    this._header = new HtmlRow(
      headers.map((header, idx) => new HtmlCell(header, { isHeader: idx === 0, scope: 'col' }))
    );

    return this;
  }

  public withRow(cells: HtmlCell[]) {
    this._rows.push(new HtmlRow(cells));

    return this;
  }

  public withRows(rows: HtmlCell[][]) {
    this._rows.push(...rows.map(cells => new HtmlRow(cells)));

    return this;
  }

  public withEmptyRow(cells?: number) {
    if (this._rows.length === 0) {
      return this;
    }

    const lastRow = this._rows[this._rows.length - 1];
    const numberOfCells = cells === undefined ? lastRow.cells.length : cells;
    const actualCells: HtmlCell[] = [];

    for (let i = 0; i < numberOfCells; i++) {
      actualCells.push(new HtmlCell(''));
    }

    this._rows.push(new HtmlRow(actualCells));

    return this;
  }

  public render() {
    const header = this._header.render();
    const rows = this._rows.map(row => row.render());

    const table = new TableElement();
    const thead = new TableHeadElement();
    const tbody = new TableBodyElement();

    table.addClass('table').addClass('table-striped');

    table.addHead(thead);
    table.addBody(tbody);

    thead.addRow(header);
    tbody.addRow(...rows);

    return table;
  }
}

export function createTable({ header, rows, footer, aggregate }: TableFactoryOptions) {
  const renderer = new HtmlTableRenderer()
    .withHeader(header)
    .withRows(
      rows.map(row =>
        row.map((cell, idx) => new HtmlCell(cell, { scope: idx === 0 ? 'row' : undefined, isHeader: idx === 0 }))
      )
    )
    .withRow(
      footer.map(
        (cell, idx) => new HtmlCell(cell, { bold: true, scope: idx === 0 ? 'row' : undefined, isHeader: idx === 0 })
      )
    );

  if (aggregate) {
    renderer
      .withEmptyRow()
      .withRows(
        aggregate.map(row =>
          row.map((cell, idx) => new HtmlCell(cell, { scope: idx === 0 ? 'row' : undefined, isHeader: idx === 0 }))
        )
      );
  }

  return renderer.render();
}
