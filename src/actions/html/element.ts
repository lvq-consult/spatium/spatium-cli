import { TextContent } from './elements/text-content.element';
import { ElementType } from './interfaces/element-type.interface';
import { ElementAttributes } from './types/element-attributes.type';
import { TextContentPlacement } from './types/text-content-placement.type';

export class Element extends TextContent {
  private classes: string[] = [];
  private children: Element[] = [];
  private attributes = new Map<ElementAttributes, string>();

  constructor(
    public tag: string,
    public textContent?: string,
    public textContentPlacement: TextContentPlacement = 'prefix'
  ) {
    super(textContent);
  }

  public render() {
    const textContent = super.render();
    const attributes = new Map<ElementAttributes, string>();

    if (this.classes.length > 0) {
      const classes = this.classes.join(' ');

      attributes.set('class', classes);
    }

    this.attributes.forEach((value, key) => {
      if (!this.attributes.has(key)) {
        attributes.set(key, value);
      }

      const existing = attributes.get(key);

      if (existing) {
        attributes.set(key, `${existing} ${value}`);
      } else {
        attributes.set(key, value);
      }
    });

    const attr = [];

    for (const kv of attributes.entries()) {
      attr.push(kv);
    }

    const attrStr = attr.map(([key, value]) => `${key}="${value}"`).join(' ');
    const children = this.children.map(child => child.render());
    const content = [];

    if (this.textContentPlacement === 'prefix') {
      content.push(textContent);
    }

    if (children.length > 0) {
      content.push(...children);
    }

    if (this.textContentPlacement === 'suffix') {
      content.push(textContent);
    }

    return this.afterRender(
      `<${this.tag}${attrStr.length > 0 ? ' ' + attrStr : ''}>${content.join('\n')}</${this.tag}>`
    );
  }

  public addClass(value: string) {
    if (!this.classes.includes(value)) {
      this.classes.push(value);
    }

    return this;
  }

  public removeClass(value: string) {
    if (this.hasClass(value)) {
      this.classes.splice(this.classes.indexOf(value), 1);
    }

    return this;
  }

  public hasClass(value: string) {
    return this.classes.includes(value);
  }

  public setAttribute(key: ElementAttributes, value: string) {
    this.attributes.set(key, value);

    return this;
  }

  public getAttribute(key: ElementAttributes) {
    return this.attributes.get(key);
  }

  public hasAttribute(key: ElementAttributes) {
    return this.attributes.has(key);
  }

  public removeAttribute(key: ElementAttributes) {
    return this.attributes.delete(key);
  }

  public addChild(...child: Element[]) {
    this.children.push(...child);

    return this;
  }

  public removeChild(index: number) {
    this.children.splice(index, 1);

    return this;
  }

  public getChildren() {
    return this.children;
  }

  public getChildrenOfType<TChild extends Element>(type: ElementType<TChild>): TChild[] {
    return this.children.filter(child => child instanceof type) as TChild[];
  }

  public getChild(index: number) {
    return this.children[index];
  }
}
