import { Element } from '../element';
import { TableBodyElement } from './table-body.element';
import { TableHeadElement } from './table-head.element';

export class TableElement extends Element {
  public get bodies() {
    return this.getChildrenOfType(TableBodyElement);
  }

  public get headers() {
    return this.getChildrenOfType(TableHeadElement);
  }

  constructor() {
    super('table');
  }

  public addBody(body: TableBodyElement) {
    this.addChild(body);
  }

  public addHead(body: TableHeadElement) {
    this.addChild(body);
  }
}
