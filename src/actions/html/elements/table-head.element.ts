import { TableContentsElement } from './table-contents.element';

export class TableHeadElement extends TableContentsElement {
  constructor() {
    super('thead');
  }
}
