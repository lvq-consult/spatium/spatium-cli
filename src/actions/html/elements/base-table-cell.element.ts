import { Element } from '../element';
import { TableCellScope } from '../types/table-cell-scope.type';
import { TableCellTags } from '../types/table-cell-tags.type';
import { TextContentPlacement } from '../types/text-content-placement.type';

export abstract class BaseTableCellElement extends Element {
  public get scope(): TableCellScope {
    return this.hasScope() ? this.getScope() : undefined;
  }

  constructor(
    public tag: TableCellTags,
    public textContent?: string,
    public textContentPlacement: TextContentPlacement = 'prefix'
  ) {
    super(tag, textContent, textContentPlacement);
  }

  public setScope(value: TableCellScope) {
    this.setAttribute('scope', value);
  }

  public hasScope() {
    return this.hasAttribute('scope');
  }

  public getScope() {
    return this.getAttribute('scope') as TableCellScope;
  }

  public removeScope() {
    this.removeAttribute('scope');
  }
}
