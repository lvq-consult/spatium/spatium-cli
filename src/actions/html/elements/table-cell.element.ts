import { TextContentPlacement } from '../types/text-content-placement.type';
import { BaseTableCellElement } from './base-table-cell.element';

export class TableCellElement extends BaseTableCellElement {
  constructor(public textContent?: string, public textContentPlacement: TextContentPlacement = 'prefix') {
    super('td', textContent, textContentPlacement);
  }
}
