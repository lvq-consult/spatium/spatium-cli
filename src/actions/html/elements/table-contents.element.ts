import { Element } from '../element';
import { TableRowElement } from './table-row.element';

export class TableContentsElement extends Element {
  public get rows() {
    return this.getChildrenOfType(TableRowElement);
  }

  constructor(public tag: 'tbody' | 'thead') {
    super(tag);
  }

  public addRow(...rows: TableRowElement[]) {
    this.addChild(...rows);
  }
}
