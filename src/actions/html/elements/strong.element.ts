import { Element } from '../element';
import { TextContentPlacement } from '../types/text-content-placement.type';

export class StrongElement extends Element {
  constructor(public textContent: string, public textContentPlacement: TextContentPlacement = 'prefix') {
    super('strong', textContent, textContentPlacement);
  }

  public render() {
    if (this.textContent && this.textContent.length > 0) {
      return super.render();
    } else {
      return '';
    }
  }
}
