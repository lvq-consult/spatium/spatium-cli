export class TextContent {
  constructor(public textContent?: string) {}

  public render() {
    return this.afterRender(this.textContent ? this.textContent : '');
  }

  protected afterRender(html: string) {
    return html;
  }
}
