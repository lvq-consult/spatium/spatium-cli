import { Element } from '../element';
import { TableCellElement } from './table-cell.element';
import { TableHeaderCellElement } from './table-header-cell.element';

export class TableRowElement extends Element {
  public get cells() {
    return [...this.getChildrenOfType(TableHeaderCellElement), ...this.getChildrenOfType(TableCellElement)];
  }

  constructor() {
    super('tr');
  }

  public addCell(...cell: TableCellElement[]) {
    this.addChild(...cell);
  }
}
