import { TableContentsElement } from './table-contents.element';

export class TableBodyElement extends TableContentsElement {
  constructor() {
    super('tbody');
  }
}
