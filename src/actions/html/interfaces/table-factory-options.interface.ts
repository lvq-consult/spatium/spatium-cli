export interface TableFactoryOptions {
  header?: string[];
  rows?: (string | number)[][];
  footer?: (string | number)[];
  aggregate?: (string | number)[][];
}
