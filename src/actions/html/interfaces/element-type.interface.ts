import { Element } from '../element';

export interface ElementType<T extends Element> {
  new (...args: any[]): T;
  [property: string]: any;
}
