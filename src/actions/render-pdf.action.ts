import { BrowserConnectOptions, BrowserLaunchArgumentOptions, LaunchOptions, PDFOptions, Product } from 'puppeteer';
import { injectable } from 'tsyringe';
// WORKAROUND: https://github.com/puppeteer/puppeteer/issues/7529#issuecomment-948926701
import * as puppeteer from 'puppeteer';

export interface PuppeteerOptions {
  emulateMedia?: 'screen' | 'print';
  launch?: LaunchOptions &
    BrowserLaunchArgumentOptions &
    BrowserConnectOptions & {
      product?: Product;
      extraPrefsFirefox?: Record<string, unknown>;
    };
  pdf?: PDFOptions;
}

@injectable()
export class RenderPdfAction {
  public defaultOptions: PuppeteerOptions = {
    emulateMedia: 'print',
    launch: {},
    pdf: {
      format: 'a4',
    },
  };

  constructor() {}

  public withOptions(options: PuppeteerOptions = {}) {
    this.defaultOptions.launch = {
      ...this.defaultOptions.launch,
      ...options.launch,
    };

    this.defaultOptions.pdf = {
      ...this.defaultOptions.pdf,
      ...options.pdf,
    };

    if (options.emulateMedia) {
      this.defaultOptions.emulateMedia = options.emulateMedia;
    }

    return this;
  }

  public async renderFromHtml(html: string) {
    const { browser, page } = await this.getBrowserAndPage();

    await page.setContent(html);

    const buffer = await page.pdf({
      ...this.defaultOptions.pdf,
    });

    await browser.close();

    return buffer;
  }

  /**
   *
   * @param path The file path to save the PDF to. If path is a relative path, then it is resolved relative to current
   * working directory. If no path is provided, the PDF won't be saved to the disk.
   */
  public async renderFromHtmlToFile(html: string, path: string) {
    const { browser, page } = await this.getBrowserAndPage();

    await page.setContent(html);

    await page.pdf({
      ...this.defaultOptions.pdf,
      path,
    });
    await browser.close();
  }

  private async getBrowserAndPage() {
    const browser = await puppeteer.launch({
      ...this.defaultOptions.launch,
    });
    const page = await browser.newPage();

    if (this.defaultOptions?.emulateMedia) {
      await page.emulateMediaType(this.defaultOptions.emulateMedia);
    }

    return { browser, page };
  }
}
