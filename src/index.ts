import 'reflect-metadata';
import { container } from 'tsyringe';
import * as yargs from 'yargs';
import { Arguments } from 'yargs';
import { AddAgentCommand } from './commands/agents/add-agent.command';
import { DeleteAgentCommand } from './commands/agents/delete-agent.command';
import { EditAgentCommand } from './commands/agents/edit-agent.command';
import { AddAssignmentCommand } from './commands/assignments/add-assignment.command';
import { DeleteAssignmentCommand } from './commands/assignments/delete-assignment.command';
import { EditAssignmentCommand } from './commands/assignments/edit-assignment.command';
import { BaseCommand } from './commands/base.command';
import { AddCustomerCommand } from './commands/customers/add-customer.command';
import { DeleteCustomerCommand } from './commands/customers/delete-customer.command';
import { EditCustomerCommand } from './commands/customers/edit-customer.command';
import { ReindexCommand } from './commands/db/reindex.command';
import { CvrApiConfigCommand } from './commands/integrate/cvr-api/config.command';
import { DropboxLoginCommand } from './commands/integrate/dropbox/login.command';
import { GitConfigCommand } from './commands/integrate/git/config.command';
import { MigrateCommand } from './commands/migrate/migrate.command';
import { ExportCommand } from './commands/reports/export.command';
import { ImportCommand } from './commands/reports/import.command';
import { MonthlyReportCommand } from './commands/reports/monthly-report.command';
import { AddTimesheetCommand } from './commands/timesheets/add-timesheet.command';
import { DeleteTimesheetCommand } from './commands/timesheets/delete-timesheet.command';
import { EditTimesheetCommand } from './commands/timesheets/edit-timesheet.command';
import { AddUserCommand } from './commands/users/add-user.command';
import { DeleteUserCommand } from './commands/users/delete-user.command';
import { EditUserCommand } from './commands/users/edit-user.command';
import { ModuleWrapper } from './module-wrapper';
import { hasMigrated } from './repositories/infrastructure/has-migrated.fn';
import { LogService } from './services/log.service';

export function getHandler<T, U>(command: BaseCommand<T, U>) {
  return (args: Arguments<U>) => {
    return command.handler(args);
  };
}

function assertMigrationStatus() {
  if (!hasMigrated('agents')) {
    throw new Error(`'agents' needs to be migrated, please run 'spatium-cli migrate'`);
  }
  if (!hasMigrated('assignment')) {
    throw new Error(`'assignment' needs to be migrated, please run 'spatium-cli migrate'`);
  }
  if (!hasMigrated('config')) {
    throw new Error(`'config' needs to be migrated, please run 'spatium-cli migrate'`);
  }
  if (!hasMigrated('customers')) {
    throw new Error(`'customers' needs to be migrated, please run 'spatium-cli migrate'`);
  }
  if (!hasMigrated('happiness')) {
    throw new Error(`'happiness' needs to be migrated, please run 'spatium-cli migrate'`);
  }
  if (!hasMigrated('timesheets')) {
    throw new Error(`'timesheets' needs to be migrated, please run 'spatium-cli migrate'`);
  }
  if (!hasMigrated('users')) {
    throw new Error(`'users' needs to be migrated, please run 'spatium-cli migrate'`);
  }
}

function wrap<T, U, TCommand extends BaseCommand<T, U>>(commandType: new (...args: any[]) => TCommand) {
  const command = container.resolve(commandType);

  return new ModuleWrapper(command);
}

export async function run() {
  try {
    assertMigrationStatus();

    // tslint:disable-next-line:no-unused-expression
    yargs
      .option('verbose', { default: false })
      .middleware(args => {
        if (args.verbose) {
          container.resolve(LogService).unsilence();
        }
      })
      .command(wrap(AddCustomerCommand))
      .command(wrap(AddTimesheetCommand))
      .command(wrap(AddUserCommand))
      .command(wrap(AddAssignmentCommand))
      .command(wrap(AddAgentCommand))
      .command(wrap(EditCustomerCommand))
      .command(wrap(EditTimesheetCommand))
      .command(wrap(EditUserCommand))
      .command(wrap(EditAssignmentCommand))
      .command(wrap(EditAgentCommand))
      .command(wrap(DeleteCustomerCommand))
      .command(wrap(DeleteTimesheetCommand))
      .command(wrap(DeleteUserCommand))
      .command(wrap(DeleteAssignmentCommand))
      .command(wrap(DeleteAgentCommand))
      .command(wrap(ExportCommand))
      .command(wrap(ImportCommand))
      .command(wrap(MonthlyReportCommand))
      .command(wrap(DropboxLoginCommand))
      .command(wrap(GitConfigCommand))
      .command(wrap(MigrateCommand))
      .command(wrap(ReindexCommand))
      .command(wrap(CvrApiConfigCommand))
      .command(
        'help',
        'Shows the help',
        b => b,
        () => yargs.showHelp()
      )
      .help()
      .wrap(120).argv;
  } catch (error) {
    console.error(error);
    process.exit(-1);
  }
}
