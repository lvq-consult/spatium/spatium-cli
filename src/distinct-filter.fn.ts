export function distinctFilter<TValue>(value: TValue, idx: number, self: TValue[]) {
  return self.indexOf(value) === idx;
}
