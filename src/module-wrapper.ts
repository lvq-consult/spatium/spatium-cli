import { Arguments, CommandBuilder, CommandModule } from 'yargs';

export class ModuleWrapper<T = {}, U = {}> implements CommandModule<T, U> {
  public aliases?: string | ReadonlyArray<string> = this.base.aliases;
  public builder?: CommandBuilder<T, U> = this.base.builder;
  public command?: string | ReadonlyArray<string> = this.base.command;
  public describe?: string | false = this.base.describe;
  public handler: (args: Arguments<U>) => void = args => this.base.handler(args);

  constructor(private base: CommandModule<T, U>) {}
}
