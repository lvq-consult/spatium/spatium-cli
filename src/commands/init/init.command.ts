import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ConfigRepository } from '../../repositories/config.repository';
import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

@injectable()
export class InitCommand extends BaseCommand implements CommandModule {
  public command = 'init';
  public aliases = 'i';
  public describe = 'Initializes a new repo';

  constructor(public database: SpatiumDatabase, public config: ConfigRepository, public dateHelper: DateHelperService) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {}
}
