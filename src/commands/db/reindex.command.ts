import { MultiBar, Presets } from 'cli-progress';
import { injectable } from 'tsyringe';
import { CommandModule } from 'yargs';
import { ConfigRepository } from '../../repositories/config.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

@injectable()
export class ReindexCommand extends BaseCommand implements CommandModule {
  public command = 'db:reindex';
  public describe = '';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: { [argName: string]: unknown; _: string[]; $0: string }) {
    const tables = this.database.getTables();
    const progressBar = new MultiBar(
      {
        format: `> {table}{bar} {percentage}% - ETA: {eta_formatted}s - {value}/{total} entries`,
        hideCursor: true,
        autopadding: true,
        fps: 30,
      },
      Presets.rect
    );
    const promises: Promise<void>[] = [];

    for (const table of tables) {
      const total = await table.count();
      const tableName = table.tableName;
      const bar = progressBar.create(total, 0, {
        table: this.normalizeTableName(tableName),
      });

      promises.push(
        table.reindex(
          () => {
            bar.increment();
            bar.updateETA();
          },
          () => bar.stop()
        )
      );
    }

    await Promise.all(promises);

    progressBar.stop();
  }

  private normalizeTableName(str: string) {
    const subStr = str.slice(0, 13);

    if (subStr.length <= 5) {
      return `${subStr}\t\t`;
    } else if (subStr.length > 5 && subStr.length < 13) {
      return `${subStr}\t`;
    } else {
      return subStr;
    }
  }
}
