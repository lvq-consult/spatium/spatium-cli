import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { AgentRepository } from '../../repositories/agent.repository';
import { ConfigRepository } from '../../repositories/config.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

@injectable()
export class AddAgentCommand extends BaseCommand implements CommandModule {
  public command = 'add:agent';
  public aliases = 'aa';
  public describe = 'Add a new agent';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public agents: AgentRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const { name, cvr } = await prompts([
      {
        type: 'text',
        name: 'name',
        message: 'Name:',
      },
      {
        type: 'text',
        name: 'cvr',
        message: 'CVR:',
      },
    ]);

    if (!name || !cvr) {
      this.abort('Missing input, aborting...');
    }

    await this.agents.create({ name, cvr });
  }
}
