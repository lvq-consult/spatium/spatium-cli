import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { AgentRepository } from '../../repositories/agent.repository';
import { ConfigRepository } from '../../repositories/config.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

@injectable()
export class EditAgentCommand extends BaseCommand implements CommandModule {
  public command = 'edit:agent';
  public aliases = 'ea';
  public describe = 'Edit a agent';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public agents: AgentRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const { id } = await prompts([
      {
        type: 'select',
        name: 'id',
        message: 'Pick an agent you want to edit:',
        choices: (
          await this.agents.all()
        ).map(({ id: value, name: customerName, cvr: customerCvr }) => ({
          title: `${customerName} (${customerCvr})`,
          value,
        })),
      },
    ]);

    const agent = await this.agents.findOne(id);

    if (!agent) {
      this.abort('Unable to find agent');
    }

    const { name, cvr } = await prompts([
      {
        type: 'text',
        name: 'name',
        message: 'Name:',
        initial: agent.name,
      },
      {
        type: 'text',
        name: 'cvr',
        message: 'CVR:',
        initial: agent.cvr,
      },
    ]);

    if (!name || !cvr) {
      this.abort('Missing input, aborting...');
    }

    await this.agents.update(id, { name, cvr });
  }
}
