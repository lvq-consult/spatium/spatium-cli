import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { AgentRepository } from '../../repositories/agent.repository';
import { ConfigRepository } from '../../repositories/config.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

@injectable()
export class DeleteAgentCommand extends BaseCommand implements CommandModule {
  public command = 'delete:agent';
  public aliases = 'da';
  public describe = 'Delete a agent';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public agents: AgentRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const { id } = await prompts([
      {
        type: 'select',
        name: 'id',
        message: 'Pick an agent you want to delete:',
        choices: (
          await this.agents.all()
        ).map(({ id: value, name, cvr }) => ({
          title: `${name} (${cvr})`,
          value,
        })),
      },
    ]);

    if (!id) {
      this.abort('Missing input, aborting...');
    }

    await this.agents.delete(id);
  }
}
