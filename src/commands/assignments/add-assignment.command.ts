import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { AgentRepository } from '../../repositories/agent.repository';
import { AssignmentRepository } from '../../repositories/assignment.repository';
import { ConfigRepository } from '../../repositories/config.repository';
import { CustomerRepository } from '../../repositories/customer.repository';
import { UserRepository } from '../../repositories/user.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

@injectable()
export class AddAssignmentCommand extends BaseCommand implements CommandModule {
  public command = 'add:assignment';
  public aliases = 'aas';
  public describe = 'Add a new assignment';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public users: UserRepository,
    public customers: CustomerRepository,
    public agents: AgentRepository,
    public assignments: AssignmentRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const { customerId, agentId, userId } = await prompts([
      {
        type: 'select',
        name: 'customerId',
        message: 'Pick a customer:',
        choices: (
          await this.customers.all()
        ).map(({ id: value, name: customerName, cvr: customerCvr }) => ({
          title: `${customerName} (${customerCvr})`,
          value,
        })),
      },
      {
        type: 'select',
        name: 'agentId',
        message: 'Pick an agent you want to edit:',
        choices: (
          await this.agents.all()
        ).map(({ id: value, name: customerName, cvr: customerCvr }) => ({
          title: `${customerName} (${customerCvr})`,
          value,
        })),
      },
      {
        type: 'select',
        name: 'userId',
        message: 'Pick a user you want to edit:',
        choices: (
          await this.users.all()
        ).map(({ id: value, username, firstname, lastname }) => ({
          title: `${firstname} ${lastname} (${username})`,
          value,
        })),
      },
    ]);

    if (!customerId || !agentId || !userId) {
      this.abort('Missing inputs');
    }

    await this.assignments.create({
      customerId,
      agentId,
      userId,
    });
  }
}
