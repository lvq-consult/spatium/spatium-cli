import { MultiBar, Presets } from 'cli-progress';
import { mkdirp } from 'fs-extra';
import { injectable } from 'tsyringe';
import { Arguments, CommandBuilder, CommandModule } from 'yargs';
import { Agent } from '../../models/agent.interface';
import { Assignment } from '../../models/assignment.interface';
import { BaseModel } from '../../models/base.interface';
import { Customer } from '../../models/customer.interface';
import { Happiness } from '../../models/happiness.interface';
import { SpatiumConfigBaseEntity } from '../../models/spatium-config.interface';
import { Timesheet } from '../../models/timesheet.interface';
import { User } from '../../models/user.interface';
import { ConfigRepository } from '../../repositories/config.repository';
import { getDataPath } from '../../repositories/infrastructure/base.repository';
import { hasMigrated } from '../../repositories/infrastructure/has-migrated.fn';
import { LegacyBaseRepositoryHandler } from '../../repositories/infrastructure/legacy-base-repository-handler';
import { Migrater } from '../../repositories/infrastructure/migrater';
import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

export interface MigrateCommandArguments {
  clean: boolean;
}

@injectable()
export class MigrateCommand
  extends BaseCommand<{}, MigrateCommandArguments>
  implements CommandModule<{}, MigrateCommandArguments>
{
  public command = 'migrate';
  public describe = '';
  public builder: CommandBuilder<{}, MigrateCommandArguments> = {
    clean: {
      default: false,
      alias: 'c',
      describe: 'Remove the old data files',
      boolean: true,
    },
  };

  constructor(public database: SpatiumDatabase, public config: ConfigRepository, public dateHelper: DateHelperService) {
    super(database, config, dateHelper);
  }

  protected async beforeExecute() {
    await this.dateHelper.ensureSyncDefaults();

    return Promise.resolve();
  }

  protected async execute(args: Arguments<MigrateCommandArguments>) {
    if (await this.hasMigratedEverything()) {
      console.log('Congratulations! Everything has already been migrated.');
      process.exit(0);
    }

    console.log('> Preparing old data path');
    await mkdirp(getDataPath());

    let config: LegacyBaseRepositoryHandler<SpatiumConfigBaseEntity<any, any>>;
    if (!(await hasMigrated('config'))) {
      console.log('> Preparing config repository');
      config = new LegacyBaseRepositoryHandler<SpatiumConfigBaseEntity<any, any>>('config');
      await config.ready;
    } else {
      console.log(`> config was already migrated`);
    }

    let customers: LegacyBaseRepositoryHandler<Customer>;
    if (!(await hasMigrated('customers'))) {
      console.log('> Preparing customers repository');
      customers = new LegacyBaseRepositoryHandler<Customer>('customers');
      await customers.ready;
    } else {
      console.log(`> customers was already migrated`);
    }

    let timesheets: LegacyBaseRepositoryHandler<Timesheet>;
    if (!(await hasMigrated('timesheets'))) {
      console.log('> Preparing timesheets repository');
      timesheets = new LegacyBaseRepositoryHandler<Timesheet>('timesheets');
      await timesheets.ready;
    } else {
      console.log(`> timesheets was already migrated`);
    }

    let agents: LegacyBaseRepositoryHandler<Agent>;
    if (!(await hasMigrated('agents'))) {
      console.log('> Preparing agents repository');
      agents = new LegacyBaseRepositoryHandler<Agent>('agents');
      await agents.ready;
    } else {
      console.log(`> agents was already migrated`);
    }

    let assignments: LegacyBaseRepositoryHandler<Assignment>;
    if (!(await hasMigrated('assignments'))) {
      console.log('> Preparing assignments repository');
      assignments = new LegacyBaseRepositoryHandler<Assignment>('assignments');
      await assignments.ready;
    } else {
      console.log(`> assignments was already migrated`);
    }

    let users: LegacyBaseRepositoryHandler<User>;
    if (!(await hasMigrated('users'))) {
      console.log('> Preparing users repository');
      users = new LegacyBaseRepositoryHandler<User>('users');
      await users.ready;
    } else {
      console.log(`> users was already migrated`);
    }

    let happiness: LegacyBaseRepositoryHandler<Happiness>;
    if (!(await hasMigrated('happiness'))) {
      console.log('> Preparing happiness repository');
      happiness = new LegacyBaseRepositoryHandler<Happiness>('happiness');
      await happiness.ready;
    } else {
      console.log(`> happiness was already migrated`);
    }

    const progressBar = new MultiBar(
      {
        format: `> {table}{bar} {percentage}% - ETA: {eta_formatted}s - {value}/{total} entries`,
        hideCursor: true,
        autopadding: true,
        fps: 30,
      },
      Presets.rect
    );

    await Promise.all([
      config ? this.migrate(config, progressBar, args.clean) : Promise.resolve(),
      customers ? this.migrate(customers, progressBar, args.clean) : Promise.resolve(),
      timesheets ? this.migrate(timesheets, progressBar, args.clean) : Promise.resolve(),
      agents ? this.migrate(agents, progressBar, args.clean) : Promise.resolve(),
      assignments ? this.migrate(assignments, progressBar, args.clean) : Promise.resolve(),
      users ? this.migrate(users, progressBar, args.clean) : Promise.resolve(),
      happiness ? this.migrate(happiness, progressBar, args.clean) : Promise.resolve(),
    ]);

    progressBar.stop();
  }

  private async migrate<T extends BaseModel>(
    repo: LegacyBaseRepositoryHandler<T>,
    progressBar: MultiBar,
    clean: boolean
  ) {
    const migrater = new Migrater<T>(repo);
    const alreadyMigrated = await migrater.hasMigrated();

    if (!alreadyMigrated) {
      const total = repo.all().length;
      const bar = progressBar.create(total, 0, {
        table: this.normalizeTableName(repo.name),
      });

      await migrater.migrate(() => {
        bar.increment();
        bar.updateETA();
      }, clean);

      bar.stop();
    }
  }

  private normalizeTableName(str: string) {
    const subStr = str.slice(0, 13);

    if (subStr.length <= 5) {
      return `${subStr}\t\t`;
    } else if (subStr.length > 5 && subStr.length < 13) {
      return `${subStr}\t`;
    } else {
      return subStr;
    }
  }

  private async hasMigratedEverything() {
    const result = await Promise.all([
      hasMigrated('config'),
      hasMigrated('customers'),
      hasMigrated('timesheets'),
      hasMigrated('agents'),
      hasMigrated('assignments'),
      hasMigrated('users'),
      hasMigrated('happiness'),
    ]);

    return result.every(h => h === true);
  }
}
