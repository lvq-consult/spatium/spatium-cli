import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { Timesheet } from '../../models/timesheet.interface';
import { ConfigRepository } from '../../repositories/config.repository';
import { CustomerRepository } from '../../repositories/customer.repository';
import { HappinessRepository } from '../../repositories/happiness.repository';
import { TimesheetRepository } from '../../repositories/timesheet.repository';
import { UserRepository } from '../../repositories/user.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseTimesheetCommand } from './base-timesheet.command';

@injectable()
export class AddTimesheetCommand extends BaseTimesheetCommand implements CommandModule {
  public command = 'add:timesheet';
  public aliases = 'at';
  public describe = 'Add a new timesheet';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public users: UserRepository,
    public customers: CustomerRepository,
    public timesheets: TimesheetRepository,
    public happiness: HappinessRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(_args: Arguments<{}>) {
    const allUsers = await this.users.all();
    const allCustomers = await this.customers.all();

    if (allUsers.length === 0) {
      console.log('Please add at least one user');
      process.exit(-1);
    }

    if (allCustomers.length === 0) {
      console.log('Please add at least one customer');
      process.exit(-1);
    }

    const userChoices = allUsers.map(({ id, firstname, lastname, username }) => ({
      title: `${firstname} ${lastname} (${username})`,
      value: id,
    }));
    const { userId } = await prompts([
      {
        type: 'select',
        name: 'userId',
        message: 'User:',
        choices: userChoices,
        initial: this.lastUserId ? userChoices.findIndex(c => c.value === this.lastUserId) : 0,
      },
    ]);
    const customerChoices = allCustomers.map(({ id, name, cvr }) => ({
      title: `${name} (${cvr})`,
      value: id,
    }));
    const { customerId } = await prompts([
      {
        type: 'select',
        name: 'customerId',
        message: 'Customer:',
        choices: customerChoices,
        initial: this.lastCustomerId ? customerChoices.findIndex(c => c.value === this.lastCustomerId) : 0,
      },
    ]);
    this.config.set('lastSelection', { customerId, userId });
    const bestTaskMatch = await this.timesheets.latestTask(customerId);
    const tasks = await this.timesheets.pluckWhere('task', 'customerId', customerId);
    const data = await prompts([
      {
        type: 'text',
        name: 'date',
        message: 'Date:',
        initial: await this.dateHelper.todayISO(),
        format: input => this.dateHelper.formatToISOSync(input),
        validate: input => this.dateHelper.isValidDateSync(input),
      },
      {
        type: 'toggle',
        name: 'isNewTask',
        message: 'Did you work on a new task?',
        initial: false,
        active: 'yes',
        inactive: 'no',
      },
      {
        type: prev => (prev === false ? 'autocomplete' : null),
        name: 'task',
        message: 'Task:',
        initial: bestTaskMatch?.task ?? '',
        validate: value => value && value.toString().length > 0,
        choices: tasks.map(task => ({ title: task, value: task, task })),
        suggest: (input, choices) =>
          Promise.resolve([...choices.filter(choice => choice?.value?.includes(input) === true)]),
      },
      {
        type: prev => (prev === true ? 'text' : null),
        name: 'task',
        message: 'Task:',
        initial: bestTaskMatch?.task ?? '',
        validate: value => value && value.toString().length > 0,
      },
      {
        type: 'number',
        name: 'hours',
        message: 'Hours:',
        initial: 8,
        validate: value => value >= 0,
      },
      {
        type: 'number',
        name: 'minutes',
        message: 'Minutes:',
        initial: 0,
        validate: value => value >= 0 && value <= 60,
      },
      {
        type: 'text',
        name: 'note',
        message: 'Note:',
      },
      {
        type: 'select',
        name: 'mood',
        message: 'How do you feel about todays work?',
        choices: [
          { title: 'Happy', value: 'happy' },
          { title: 'Stressed', value: 'stressed' },
          { title: 'Sad', value: 'sad' },
          { title: 'Angry', value: 'angry' },
          { title: 'Bored', value: 'bored' },
          { title: 'Meh', value: 'meh' },
        ],
        initial: 0,
      },
      {
        type: 'number',
        name: 'score',
        message: 'On a scale from 0-100, how do you feel today?',
        initial: 50,
        style: 'default',
        min: -100,
        max: 100,
      },
    ]);

    const timesheet = {
      userId,
      customerId,
      date: data.date,
      task: data.task,
      hours: data.hours,
      minutes: data.minutes,
      note: data.note,
    } as Timesheet;
    const { score, mood } = data;

    if (!this.allPropertiesSet(timesheet) || !mood || score === null || score === undefined) {
      this.abort('Missing input, aborting...');
    }

    const { id: timesheetId } = await this.timesheets.create({
      ...timesheet,
    } as Timesheet);

    await this.happiness.create({
      timesheetId,
      score,
      mood,
    });
  }
}
