import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ConfigRepository } from '../../repositories/config.repository';
import { CustomerRepository } from '../../repositories/customer.repository';
import { HappinessRepository } from '../../repositories/happiness.repository';
import { TimesheetRepository } from '../../repositories/timesheet.repository';
import { UserRepository } from '../../repositories/user.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseTimesheetCommand } from './base-timesheet.command';

@injectable()
export class DeleteTimesheetCommand extends BaseTimesheetCommand implements CommandModule {
  public command = 'delete:timesheet';
  public aliases = 'dt';
  public describe = 'Delete a timesheet';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public users: UserRepository,
    public customers: CustomerRepository,
    public timesheets: TimesheetRepository,
    public happiness: HappinessRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const { customerId } = await prompts([
      {
        type: 'select',
        name: 'customerId',
        message: 'Customer:',
        choices: (
          await this.customers.all()
        ).map(customer => ({
          title: `${customer.name} (${customer.cvr})`,
          value: customer.id,
        })),
      },
    ]);

    const timesheetChoices = await this.timesheets.find(timesheet => timesheet.customerId === customerId);

    const { id } = await prompts([
      {
        type: 'autocomplete',
        name: 'id',
        message: 'Find timesheet:',
        choices: await Promise.all(
          timesheetChoices.map(async t => ({
            title: `${await this.dateHelper.formatDate(t.date)} - ${t.task}`,
            value: t.id,
            ...t,
          }))
        ),
        suggest: (input, choices) =>
          Promise.resolve(
            choices.filter(
              (choice: any) =>
                choice.date.includes(input) ||
                this.dateHelper.formatDateSync(choice.date).includes(input) ||
                choice.task.includes(input) ||
                choice.note.includes(input)
            )
          ),
      },
    ]);

    if (!id) {
      this.abort('Missing input, aborting...');
    }

    await this.timesheets.delete(id);
    const orphanedHappiness = await this.happiness.where('timesheetId', id);

    if (!orphanedHappiness?.length) {
      return;
    }

    for (const { id: happinessId } of orphanedHappiness) {
      await this.happiness.delete(happinessId);
    }
  }
}
