import { CommandModule } from 'yargs';
import { Timesheet } from '../../models/timesheet.interface';
import { BaseCommand } from '../base.command';

export class BaseTimesheetCommand extends BaseCommand implements CommandModule {
  protected async execute(args: { [argName: string]: unknown; _: string[]; $0: string }): Promise<void> {
    return Promise.resolve();
  }

  protected allPropertiesSet(inputs: Partial<Timesheet>) {
    return inputs.customerId && inputs.date && inputs.task && inputs.hours >= 0 && inputs.minutes >= 0;
  }
}
