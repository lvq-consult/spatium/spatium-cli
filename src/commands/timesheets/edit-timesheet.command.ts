import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ConfigRepository } from '../../repositories/config.repository';
import { CustomerRepository } from '../../repositories/customer.repository';
import { HappinessRepository } from '../../repositories/happiness.repository';
import { TimesheetRepository } from '../../repositories/timesheet.repository';
import { UserRepository } from '../../repositories/user.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseTimesheetCommand } from './base-timesheet.command';

@injectable()
export class EditTimesheetCommand extends BaseTimesheetCommand implements CommandModule {
  public command = 'edit:timesheet';
  public aliases = 'et';
  public describe = 'Edit a timesheet';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public users: UserRepository,
    public customers: CustomerRepository,
    public timesheets: TimesheetRepository,
    public happiness: HappinessRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const { customerId } = await prompts([
      {
        type: 'select',
        name: 'customerId',
        message: 'Customer:',
        choices: (
          await this.customers.all()
        ).map(customer => ({
          title: `${customer.name} (${customer.cvr})`,
          value: customer.id,
        })),
      },
    ]);

    const timesheetChoices = await this.timesheets.find(t => t.customerId === customerId);
    const { id } = await prompts([
      {
        type: 'autocomplete',
        name: 'id',
        message: 'Find timesheet:',
        choices: await Promise.all(
          timesheetChoices.map(async t => ({
            title: `${await this.dateHelper.formatDate(t.date)} - ${t.task}`,
            value: t.id,
            ...t,
          }))
        ),
        suggest: (input, choices) =>
          Promise.resolve(
            choices.filter(
              (choice: any) =>
                choice.date.includes(input) ||
                this.dateHelper.formatDateSync(choice.date).includes(input) ||
                choice.task.includes(input) ||
                choice.note.includes(input)
            )
          ),
      },
    ]);

    if (!id) {
      this.abort('Missing input, aborting...');
    }

    const timesheet = await this.timesheets.findOne(id);
    const tasks = await this.timesheets.pluck('task');
    const editedTimesheet = await prompts([
      {
        type: 'text',
        name: 'date',
        message: 'Date:',
        initial: await this.dateHelper.formatDate(timesheet.date),
        format: input => this.dateHelper.formatToISOSync(input),
        validate: input => this.dateHelper.isValidDateSync(input),
      },
      {
        type: 'text',
        name: 'task',
        message: 'Task:',
        initial: timesheet.task,
        choices: tasks.map(title => ({ title, value: title })),
      },
      {
        type: 'number',
        name: 'hours',
        message: 'Hours:',
        initial: timesheet.hours,
      },
      {
        type: 'number',
        name: 'minutes',
        message: 'Minutes:',
        initial: timesheet.minutes,
        validate: value => value >= 0 && value <= 60,
      },
      {
        type: 'text',
        name: 'note',
        message: 'Note:',
        initial: timesheet.note,
      },
    ]);

    const {
      score,
      mood,
      id: happinessId,
    } = (await this.happiness.findOne(h => h.timesheetId === id)) || {
      score: 50,
      mood: 'happy',
      id: '',
    };

    const happinessChoices = [
      { title: 'Happy', value: 'happy' },
      { title: 'Stressed', value: 'stressed' },
      { title: 'Sad', value: 'sad' },
      { title: 'Angry', value: 'angry' },
      { title: 'Bored', value: 'bored' },
      { title: 'Meh', value: 'meh' },
    ];
    const editedHappiness = await prompts([
      {
        type: 'select',
        name: 'mood',
        message: 'How do you feel about todays work?',
        choices: happinessChoices,
        initial: happinessChoices.findIndex(({ value }) => value === mood),
      },
      {
        type: 'number',
        name: 'score',
        message: 'On a score from 0-100, how do you feel about today?',
        initial: score,
        style: 'default',
        min: -100,
        max: 100,
      },
    ]);

    if (
      !this.allPropertiesSet(timesheet) ||
      !editedHappiness ||
      !editedHappiness.mood ||
      editedHappiness.score === null ||
      editedHappiness.score === undefined
    ) {
      this.abort('Missing input, aborting...');
    }

    await this.timesheets.update(id, editedTimesheet);
    if (!happinessId) {
      await this.happiness.create({
        ...editedHappiness,
        timesheetId: id,
      });
    } else {
      await this.happiness.update(happinessId, editedHappiness);
    }
  }
}
