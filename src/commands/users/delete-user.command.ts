import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ConfigRepository } from '../../repositories/config.repository';
import { UserRepository } from '../../repositories/user.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseTimesheetCommand } from '../timesheets/base-timesheet.command';

@injectable()
export class DeleteUserCommand extends BaseTimesheetCommand implements CommandModule {
  public command = 'delete:user';
  public aliases = 'du';
  public describe = 'Delete a user';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public users: UserRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const { id } = await prompts([
      {
        type: 'select',
        name: 'id',
        message: 'Pick a user you want to delete:',
        choices: (
          await this.users.all()
        ).map(({ id: value, username, firstname, lastname }) => ({
          title: `${firstname} ${lastname} (${username})`,
          value,
        })),
      },
    ]);

    if (!id) {
      this.abort('Missing input, aborting...');
    }

    await this.users.delete(id);
  }
}
