import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ConfigRepository } from '../../repositories/config.repository';
import { UserRepository } from '../../repositories/user.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

@injectable()
export class AddUserCommand extends BaseCommand implements CommandModule {
  public command = 'add:user';
  public aliases = 'au';
  public describe = 'Add a new user';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public users: UserRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const { username, firstname, lastname } = await prompts([
      {
        type: 'text',
        name: 'username',
        message: 'Username:',
      },
      {
        type: 'text',
        name: 'firstname',
        message: 'First name:',
      },
      {
        type: 'text',
        name: 'lastname',
        message: 'Last name:',
      },
    ]);

    if (!username || !firstname || !lastname) {
      this.abort('Missing input, aborting...');
    }

    await this.users.create({ username, firstname, lastname });
  }
}
