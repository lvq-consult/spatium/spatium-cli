import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ConfigRepository } from '../../repositories/config.repository';
import { UserRepository } from '../../repositories/user.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

@injectable()
export class EditUserCommand extends BaseCommand implements CommandModule {
  public command = 'edit:user';
  public aliases = 'eu';
  public describe = 'Edit a user';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public users: UserRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const { id } = await prompts([
      {
        type: 'select',
        name: 'id',
        message: 'Pick a user you want to edit:',
        choices: (
          await this.users.all()
        ).map(({ id: value, username, firstname, lastname }) => ({
          title: `${firstname} ${lastname} (${username})`,
          value,
        })),
      },
    ]);

    const user = await this.users.findOne(id);

    if (!user) {
      this.abort('Unable to find user');
    }

    const editedUser = await prompts([
      {
        type: 'text',
        name: 'username',
        message: 'Username:',
        initial: user.username,
      },
      {
        type: 'text',
        name: 'firstname',
        message: 'First name:',
        initial: user.firstname,
      },
      {
        type: 'text',
        name: 'lastname',
        message: 'Last name:',
        initial: user.lastname,
      },
    ]);

    if (!editedUser.username || !editedUser.firstname || !editedUser.lastname) {
      this.abort('Missing input, aborting...');
    }

    await this.users.update(id, {
      username: editedUser.username,
      firstname: editedUser.firstname,
      lastname: editedUser.lastname,
    });
  }
}
