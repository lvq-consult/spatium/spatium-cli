import { DropboxAuth } from 'dropbox';
import * as express from 'express';
import * as fetch from 'isomorphic-fetch';
import * as opn from 'open';
import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { error, info } from '../../../logger';
import { SpatiumDropboxConfig } from '../../../models/spatium-config.interface';
import { ConfigRepository } from '../../../repositories/config.repository';
import { DateHelperService } from '../../../services/date-helper.service';
import { SpatiumDatabase } from '../../../spatium-database';
import { BaseCommand } from '../../base.command';

export interface DropboxLoginCommandArguments {
  appKey: string;
  appSecret: string;
}

@injectable()
export class DropboxLoginCommand extends BaseCommand<any, DropboxLoginCommandArguments> implements CommandModule {
  public command = 'integrate:dropbox login';
  public aliases = 'idb';
  public describe = 'Login to dropbox';
  public builder = {
    'app-key': {
      default: null,
      describe: 'Dropbox application key',
      string: true,
      alias: 'k',
    },
    'app-secret': {
      default: null,
      describe: 'Dropbox application secret',
      string: true,
      alias: 's',
    },
  };

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<DropboxLoginCommandArguments>) {
    const newCredentials = this.getArgsCredentials(args);
    const existingCredentials = await this.config.dropbox;
    let inputCredentials: { appKey: string; appSecret: string };

    if (!newCredentials && !existingCredentials) {
      inputCredentials = await prompts([
        {
          type: 'text',
          name: 'appKey',
          message: 'App Key:',
          initial: existingCredentials ? existingCredentials.appKey : undefined,
          validate: value => value && value.toString().length > 0,
        },
        {
          type: 'password',
          name: 'appSecret',
          message: 'App Secret:',
          initial: existingCredentials ? existingCredentials.appSecret : undefined,
          validate: value => value && value.toString().length > 0,
        },
      ]);

      if (!inputCredentials || !inputCredentials.appKey || !inputCredentials.appSecret) {
        this.abort('Missing input');
      }
    }

    if (inputCredentials) {
      info('if(inputCredentials)');
      await this.config.set('dropbox', {
        appKey: inputCredentials.appKey,
        appSecret: inputCredentials.appSecret,
      });
    } else if (!existingCredentials && newCredentials) {
      info('if (!existingCredentials && newCredentials)');
      await this.config.set('dropbox', {
        appKey: newCredentials.appKey,
        appSecret: newCredentials.appSecret,
      });
    } else if (!(existingCredentials && !newCredentials) && !this.same(newCredentials, existingCredentials)) {
      info('if (!(existingCredentials && !newCredentials) && !this.same(newCredentials, existingCredentials)');
      await this.config.set('dropbox', {
        appKey: newCredentials.appKey,
        appSecret: newCredentials.appSecret,
      });
    }

    const { appKey: clientId, appSecret: clientSecret } = await this.config.get('dropbox');
    const auth = new DropboxAuth({ fetch, clientId, clientSecret });
    const port = 5000;
    const redirectUrlOAuth = `http://localhost:${port}/oauth`;
    console.log('> Waiting for user action in browser...');
    return new Promise<void>((done, reject) => {
      const app = express();
      const server = app.listen(port);

      app.get('/oauth', async (req, res) => {
        const { code } = req.query;

        info('Handling request, got code', code);

        if (!code) {
          res.sendStatus(404);
        }

        try {
          const accessTokenResponse = await auth.getAccessTokenFromCode(redirectUrlOAuth, code as string);
          const accessToken = accessTokenResponse.result;
          info('Got access token', accessToken);
          await this.config.set('dropbox', { accessToken });

          console.log('> Recieved response from browser!');
          done();

          res.status(200).type('html').send(`<!doctype html>
<html>
<head>
  <title>Spatium</title>
</head>
<body>
  <h1>Everything is fine and dandy, we have authenticated!</h1>
</body>
</html>
          `);
        } catch (e) {
          error(e);
          res.sendStatus(500);
          reject();
        }

        info('Trying to stop listening');
        server.close();
      });

      server.once('listening', () => {
        info('Listning on port', port);
        opn(auth.getAuthenticationUrl(redirectUrlOAuth, '', 'code'));
      });
    });
  }

  private same(args: DropboxLoginCommandArguments, dropboxConfig: SpatiumDropboxConfig) {
    return JSON.stringify(args || null) === JSON.stringify(dropboxConfig || null);
  }

  private getArgsCredentials({ appKey, appSecret }: DropboxLoginCommandArguments) {
    if (!appKey && !appSecret) {
      return null;
    }

    return { appKey, appSecret };
  }
}
