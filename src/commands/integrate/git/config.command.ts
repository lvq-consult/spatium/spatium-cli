import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ConfigRepository } from '../../../repositories/config.repository';
import { DateHelperService } from '../../../services/date-helper.service';
import { SpatiumDatabase } from '../../../spatium-database';
import { BaseCommand } from '../../base.command';

export interface GitConfigCommandArguments {
  enabled: boolean;
}

@injectable()
export class GitConfigCommand extends BaseCommand<any, GitConfigCommandArguments> implements CommandModule {
  public command = 'integrate:git config';
  public aliases = 'igc';
  public describe = 'Setup Git config';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<GitConfigCommandArguments>) {
    const existingConfig = await this.config.git;

    const gitConfig = await prompts([
      {
        type: 'toggle',
        name: 'enabled',
        message: 'Enable git integration?',
        initial: existingConfig?.enabled ?? true,
        active: 'yes',
        inactive: 'no',
      },
      {
        type: 'text',
        name: 'name',
        message: 'Name:',
        initial: existingConfig?.name ?? '',
      },
      {
        type: 'text',
        name: 'email',
        message: 'Email:',
        initial: existingConfig?.email ?? '',
      },
      {
        type: 'text',
        name: 'publicKeyPath',
        message: 'Public Key Path:',
        initial: existingConfig?.publicKeyPath,
      },
      {
        type: 'text',
        name: 'privateKeyPath',
        message: 'Private Key Path:',
        initial: existingConfig?.privateKeyPath,
      },
    ]);

    if (gitConfig?.name && gitConfig?.name && gitConfig?.publicKeyPath && gitConfig?.privateKeyPath) {
      await this.config.set('git', {
        enabled: true,
        ...gitConfig,
      });
    } else {
      this.abort('Missing inputs.');
    }
  }
}
