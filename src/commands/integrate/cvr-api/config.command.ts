import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ConfigRepository } from '../../../repositories/config.repository';
import { DateHelperService } from '../../../services/date-helper.service';
import { SpatiumDatabase } from '../../../spatium-database';
import { BaseCommand } from '../../base.command';

export interface CvrApiConfigCommandArguments {
  enabled: boolean;
}

@injectable()
export class CvrApiConfigCommand extends BaseCommand<any, CvrApiConfigCommandArguments> implements CommandModule {
  public command = 'integrate:cvrapi config';
  public aliases = 'icac';
  public describe = 'Setup CVR API config';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<CvrApiConfigCommandArguments>) {
    const existingConfig = await this.config.cvrApi;

    const cvrApiConfig = await prompts([
      {
        type: 'toggle',
        name: 'enabled',
        message: 'Enable CVR API (https://cvrapi.dk) integration?',
        initial: existingConfig?.enabled ?? true,
        active: 'yes',
        inactive: 'no',
      },
      {
        type: 'text',
        name: 'companyName',
        message: 'Company Name:',
        initial: existingConfig?.companyName ?? '',
      },
      {
        type: 'text',
        name: 'projectName',
        message: 'Project Name:',
        initial: existingConfig?.projectName ?? '',
      },
      {
        type: 'text',
        name: 'contactName',
        message: 'Contact Name',
        initial: existingConfig?.contactName,
      },
      {
        type: 'toggle',
        name: 'useEmail',
        message: 'Preferred Contact Method:',
        initial: existingConfig?.useEmail ?? true,
        active: 'Email',
        inactive: 'Phone',
      },
      {
        type: prev => (prev === true ? 'text' : null),
        name: 'contactEmail',
        message: 'Email:',
        initial: existingConfig?.contactEmail ?? '',
      },
      {
        type: prev => (prev ? null : 'text'),
        name: 'contactPhone',
        message: 'Phone:',
        initial: existingConfig?.contactPhone ?? '',
      },
    ]);

    if (
      cvrApiConfig?.companyName &&
      cvrApiConfig?.projectName &&
      cvrApiConfig?.contactName &&
      (cvrApiConfig?.contactEmail || cvrApiConfig?.contactPhone)
    ) {
      await this.config.set('cvrApi', {
        enabled: true,
        ...cvrApiConfig,
      });
    } else {
      this.abort('Missing inputs.');
    }
  }
}
