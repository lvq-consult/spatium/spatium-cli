import * as Chartscii from 'chartscii';
import * as Table from 'cli-table3';
import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { Timesheet } from '../../models/timesheet.interface';
import { ConfigRepository } from '../../repositories/config.repository';
import { CustomerRepository } from '../../repositories/customer.repository';
import { HappinessRepository } from '../../repositories/happiness.repository';
import { TimesheetRepository } from '../../repositories/timesheet.repository';
import { UserRepository } from '../../repositories/user.repository';
import { CvrService } from '../../services/cvr-service';
import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

@injectable()
export class MonthlyReportCommand extends BaseCommand implements CommandModule {
  public command = 'report:monthly';
  public aliases = 'rm';
  public describe = 'Montly report';

  constructor(
    public database: SpatiumDatabase,
    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public users: UserRepository,
    public customers: CustomerRepository,
    public timesheets: TimesheetRepository,
    public happiness: HappinessRepository,
    public cvrService: CvrService
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const initialStartDate = await this.dateHelper.formatDateIndex(await this.dateHelper.startOfMonthDateTime());
    const customerChoices = (await this.customers.all()).map(({ id, name, cvr }) => ({
      title: `${name} (${cvr})`,
      value: id,
    }));
    const { customerId, startDateText } = await prompts([
      {
        type: 'select',
        name: 'customerId',
        message: 'Customer:',
        choices: customerChoices,
        initial: this.lastCustomerId ? customerChoices.findIndex(c => c.value === this.lastCustomerId) : 0,
      },
      {
        type: 'text',
        name: 'startDateText',
        message: 'Start Date (ISO):',
        initial: initialStartDate,
      },
    ]);

    if (customerId === undefined) {
      this.abort('Missing customer');
    }
    this.config.set('lastSelection', { customerId, userId: this.lastUserId });
    const startDate = await this.dateHelper.fromISO(startDateText);
    const dateIndex = await this.dateHelper.formatDateIndex(startDate);
    const entries = await this.timesheets.searchWhere('customerId', customerId, dateIndex, 'date');

    await this.renderChart(entries);
    await this.renderTable(entries);
  }

  public async renderTable(entries: Timesheet[]) {
    const table = new Table({
      head: ['Dato', 'Opgave', 'Timer', 'Minutter', 'Note', 'Mood', 'Happiness'],
      chars: {
        top: '═',
        'top-mid': '╤',
        'top-left': '╔',
        'top-right': '╗',
        bottom: '═',
        'bottom-mid': '╧',
        'bottom-left': '╚',
        'bottom-right': '╝',
        left: '║',
        'left-mid': '╟',
        mid: '─',
        'mid-mid': '┼',
        right: '║',
        'right-mid': '╢',
        middle: '│',
      },
      style: {
        head: ['green'],
      },
    });

    table.push(
      ...(await Promise.all(
        entries.map(async ({ id, date, task, hours: entryHours, minutes: entryMinutes, note }) => {
          const { mood, score } = (await this.happiness.findOne(h => h.timesheetId === id)) || {
            mood: '',
            score: '',
          };

          return [
            await this.dateHelper.formatDate(date),
            task ?? '',
            entryHours ?? 0,
            entryMinutes ?? 0,
            note ?? '',
            mood ?? '',
            score ?? 0,
          ];
        })
      ))
    );

    const hours = entries.reduce((sum, current) => (sum += current.hours), 0);
    const minutes = entries.reduce((sum, current) => (sum += current.minutes), 0);
    const hoursFromMinutes = Number(Math.floor(minutes / 60));
    const totalHours = hours + hoursFromMinutes;
    const minutesLeft = minutes - hoursFromMinutes * 60;

    table.push(['Total', '', totalHours, minutesLeft, '', '', '']);

    console.log(table.toString());
  }

  public async renderChart(entries: Timesheet[]) {
    const groupedByTask = new Map<string, Timesheet[]>();

    for (const timesheet of entries) {
      const task = (timesheet?.task || 'no task').toLowerCase();

      if (!groupedByTask.has(task)) {
        groupedByTask.set(task, []);
      }

      groupedByTask.get(task).push(timesheet);
    }

    const chartData = [...groupedByTask.entries()].map(
      ([task, timesheets]) =>
        ({
          value: timesheets.length,
          label: `${task} (${timesheets.length})`,
        } as Chartscii.ChartData)
    );

    const chart = new Chartscii(chartData, {
      label: 'Grouped by tasks',
      width: 120,
      char: '■',
      sort: true,
      reverse: true,
      color: 'green' as unknown as boolean, // Seems to be a bug in the typing
    });

    console.log(chart.create());
  }
}
