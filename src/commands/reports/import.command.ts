import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ConfigRepository } from '../../repositories/config.repository';
import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

interface ImportArguments {
  format: 'csv' | 'json';
  input: string;
}

@injectable()
export class ImportCommand extends BaseCommand<{}, ImportArguments> implements CommandModule<{}, ImportArguments> {
  public command = 'import';
  public aliases = 'im';
  public describe = 'Import data';
  public builder = {
    format: {
      default: 'csv',
      alias: 'f',
      description: 'Input format',
      choices: ['csv', 'json'],
      demandOption: true,
      string: true,
    },
    input: {
      alias: 'i',
      describe: 'Input file path',
      demandOption: true,
      string: true,
    },
  };

  constructor(public database: SpatiumDatabase, public config: ConfigRepository, public dateHelper: DateHelperService) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<ImportArguments>) {}
}
