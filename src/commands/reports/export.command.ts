import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ExportAction, ExportFormat, ExportFormatValues } from '../../actions/export.action';
import { PutInDropboxAction } from '../../actions/put-in-dropbox.action';
import { PutManyInDropboxAction } from '../../actions/put-many-in-dropbox.action';
import { ConfigRepository } from '../../repositories/config.repository';
import { CustomerRepository } from '../../repositories/customer.repository';
import { TimesheetRepository } from '../../repositories/timesheet.repository';
import { UserRepository } from '../../repositories/user.repository';
import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

interface ExportArguments {
  format: ExportFormat;
  directory: string;
  customer: string;
  from: string;
  to: string;
  all: boolean;
  dropbox?: boolean;
  nosandbox?: boolean;
}

@injectable()
export class ExportCommand extends BaseCommand<{}, ExportArguments> implements CommandModule<{}, ExportArguments> {
  public command = 'export';
  public aliases = 'ex';
  public describe = 'Export data';
  public builder = {
    format: {
      alias: 'f',
      describe: 'The output format',
      choices: ExportFormatValues,
      demandOption: false,
      string: true,
    },
    directory: {
      default: 'out',
      alias: 'd',
      describe: 'Output directory',
      demandOption: false,
      string: true,
    },
    customer: {
      default: null,
      alias: 'c',
      describe: 'Customer id',
      demandOption: false,
      string: true,
    },
    from: {
      default: null,
      describe: 'From date',
      string: true,
    },
    to: {
      default: null,
      describe: 'To date',
      string: true,
    },
    all: {
      default: false,
      describe: 'Export all customers in all formats',
      boolean: true,
    },
    dropbox: {
      default: false,
      describe: 'Upload to dropbox - remember to login',
      boolean: true,
    },
    nosandbox: {
      default: false,
      describe: 'Disable Chromium sandbox - useful in docker builds',
      boolean: true,
    },
  };

  constructor(
    public database: SpatiumDatabase,
    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public users: UserRepository,
    public customers: CustomerRepository,
    public timesheets: TimesheetRepository,
    public exportAction: ExportAction,
    public putManyInDropboxAction: PutManyInDropboxAction,
    public putInDropboxAction: PutInDropboxAction
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<ExportArguments>) {
    if (args.all) {
      const { directory } = args;
      const customerIds = await this.customers.pluck('id');

      if (customerIds.length === 0) {
        console.log('> Did not find any customers.');
        process.exit(0);
      }

      const start = await this.dateHelper.formatDate(await this.dateHelper.startOfMonth());
      const end = await this.dateHelper.formatDate(await this.dateHelper.endOfMonth());
      const paths = await Promise.all(
        ExportFormatValues.map(
          async format =>
            await Promise.all(
              customerIds.map(
                async customerId =>
                  await this.exportAction.execute(format, customerId, start, end, directory, args.nosandbox)
              )
            )
        )
      );
      const filepaths = paths.reduce((sum, curr) => [...sum, ...curr], []).filter(p => p !== undefined);

      if (args.dropbox && filepaths?.length > 0) {
        await this.putManyInDropboxAction.execute(await this.exportAction.getFilenamePrefix(start), filepaths);
      }
    } else {
      const questions = [
        args.customer
          ? null
          : {
              type: 'select',
              name: 'customerId',
              message: 'Customer:',
              choices: (await this.customers.all()).map(({ id, name, cvr }) => ({
                title: `${name} (${cvr})`,
                value: id,
              })),
            },
        args.from
          ? null
          : {
              type: 'date',
              name: 'fromDate',
              message: 'From:',
              mask: 'YYYY-MM-DD',
              initial: await this.dateHelper.startOfMonthDate(),
              validate: input => input !== null,
            },
        args.to
          ? null
          : {
              type: 'date',
              name: 'toDate',
              message: 'To:',
              mask: 'YYYY-MM-DD',
              initial: await this.dateHelper.endOfMonthDate(),
              validate: input => input !== null,
            },
        args.format
          ? null
          : {
              type: 'select',
              name: 'formatChoice',
              message: 'Format:',
              choices: ExportFormatValues.map(f => ({
                title: f,
                value: f,
              })),
            },
      ].filter(p => p !== null) as any;
      const { customerId, fromDate, toDate, formatChoice } = await prompts(questions);

      const from = args.from ?? this.dateHelper.formatToISOSync((fromDate as Date).toISOString());
      const to = args.to ?? this.dateHelper.formatToISOSync((toDate as Date).toISOString());

      if (!customerId || !from || !to) {
        this.abort('Missing input, aborting...');
      }

      const { format, directory } = args;

      const path = await this.exportAction.execute(
        format ?? formatChoice,
        customerId,
        from,
        to,
        directory,
        args.nosandbox
      );

      if (args.dropbox && path) {
        await this.putInDropboxAction.execute(await this.exportAction.getFilenamePrefix(from), path);
      }
    }
  }
}
