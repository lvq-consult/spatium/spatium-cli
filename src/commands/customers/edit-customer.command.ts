import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ConfigRepository } from '../../repositories/config.repository';
import { CustomerRepository } from '../../repositories/customer.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

@injectable()
export class EditCustomerCommand extends BaseCommand implements CommandModule {
  public command = 'edit:customer';
  public aliases = 'ec';
  public describe = 'Edit a customer';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public customers: CustomerRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const { id } = await prompts([
      {
        type: 'select',
        name: 'id',
        message: 'Pick a customer you want to edit:',
        choices: (
          await this.customers.all()
        ).map(({ id: value, name: customerName, cvr: customerCvr }) => ({
          title: `${customerName} (${customerCvr})`,
          value,
        })),
      },
    ]);

    const customer = await this.customers.findOne(id);

    if (!customer) {
      this.abort('Unable to find customer');
    }

    const { name, cvr } = await prompts([
      {
        type: 'text',
        name: 'name',
        message: 'Name:',
        initial: customer.name,
      },
      {
        type: 'text',
        name: 'cvr',
        message: 'CVR:',
        initial: customer.cvr,
      },
    ]);

    if (!name || !cvr) {
      this.abort('Missing input, aborting...');
    }

    await this.customers.update(id, { name, cvr });
  }
}
