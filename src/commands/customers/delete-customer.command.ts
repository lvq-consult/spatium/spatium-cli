import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ConfigRepository } from '../../repositories/config.repository';
import { CustomerRepository } from '../../repositories/customer.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

@injectable()
export class DeleteCustomerCommand extends BaseCommand implements CommandModule {
  public command = 'delete:customer';
  public aliases = 'dc';
  public describe = 'Delete a customer';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public customers: CustomerRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const { id } = await prompts([
      {
        type: 'select',
        name: 'id',
        message: 'Pick a customer you want to delete:',
        choices: (
          await this.customers.all()
        ).map(({ id: value, name, cvr }) => ({
          title: `${name} (${cvr})`,
          value,
        })),
      },
    ]);

    if (!id) {
      this.abort('Missing input, aborting...');
    }

    await this.customers.delete(id);
  }
}
