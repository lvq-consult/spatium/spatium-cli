import * as prompts from 'prompts';
import { injectable } from 'tsyringe';
import { Arguments, CommandModule } from 'yargs';
import { ConfigRepository } from '../../repositories/config.repository';
import { CustomerRepository } from '../../repositories/customer.repository';

import { DateHelperService } from '../../services/date-helper.service';
import { SpatiumDatabase } from '../../spatium-database';
import { BaseCommand } from '../base.command';

@injectable()
export class AddCustomerCommand extends BaseCommand implements CommandModule {
  public command = 'add:customer';
  public aliases = 'ac';
  public describe = 'Add a new customer';

  constructor(
    public database: SpatiumDatabase,

    public config: ConfigRepository,
    public dateHelper: DateHelperService,
    public customers: CustomerRepository
  ) {
    super(database, config, dateHelper);
  }

  protected async execute(args: Arguments<{}>) {
    const { name, cvr } = await prompts([
      {
        type: 'text',
        name: 'name',
        message: 'Name:',
      },
      {
        type: 'text',
        name: 'cvr',
        message: 'CVR:',
      },
    ]);

    if (!name || !cvr) {
      this.abort('Missing input, aborting...');
    }

    await this.customers.create({ name, cvr });
  }
}
