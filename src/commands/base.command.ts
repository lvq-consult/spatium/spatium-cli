import { Arguments, CommandModule } from 'yargs';
import { error, info } from '../logger';
import { ConfigRepository } from '../repositories/config.repository';
import { DateHelperService } from '../services/date-helper.service';
import { SpatiumDatabase } from '../spatium-database';

export abstract class BaseCommand<T = {}, U = {}> implements CommandModule<T, U> {
  public lastCustomerId: string;
  public lastUserId: string;

  constructor(
    public database: SpatiumDatabase,
    public config: ConfigRepository,
    public dateHelper: DateHelperService
  ) {}

  public async handler(args: Arguments<U>) {
    await this.beforeExecute();
    await this.execute(args);
    await this.afterExecute();
  }

  protected async beforeExecute() {
    await this.database.ensureTables();
    await this.dateHelper.ensureSyncDefaults();

    const lastSelection = await this.config.lastSelection;
    this.lastCustomerId = lastSelection?.customerId;
    this.lastUserId = lastSelection?.userId;

    return Promise.resolve();
  }

  protected async afterExecute() {
    return Promise.resolve();
  }

  protected abstract execute(args: Arguments<U>): Promise<void>;

  public abort(message: string, exitCode = 0) {
    if (exitCode === 0) {
      info(message);
    } else {
      error(new Error(message));
    }
    process.exit(exitCode);
  }
}
