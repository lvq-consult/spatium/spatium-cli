import { singleton } from 'tsyringe';
import { Happiness } from '../models/happiness.interface';

import { SpatiumDatabase } from '../spatium-database';
import { CustomerRepository } from './customer.repository';
import { BaseRepositoryV2 } from './infrastructure/base-repository-v2';
import { TimesheetRepository } from './timesheet.repository';
import { UserRepository } from './user.repository';

@singleton()
export class HappinessRepository extends BaseRepositoryV2<Happiness> {
  constructor(
    public database: SpatiumDatabase,
    public users: UserRepository,
    public customers: CustomerRepository,
    public timesheets: TimesheetRepository
  ) {
    super(database.happiness);
  }
}
