import { singleton } from 'tsyringe';
import { Customer } from '../models/customer.interface';

import { SpatiumDatabase } from '../spatium-database';
import { BaseRepositoryV2 } from './infrastructure/base-repository-v2';

@singleton()
export class CustomerRepository extends BaseRepositoryV2<Customer> {
  constructor(public database: SpatiumDatabase) {
    super(database.customers);
  }
}
