import { createReadStream } from 'fs';
import { existsSync } from 'fs-extra';
import { set as setPath } from 'lodash';
import { resolve } from 'path';
import { createInterface } from 'readline';
import { singleton } from 'tsyringe';
import { info } from '../logger';
import {
  SpatiumConfig,
  SpatiumConfigBaseEntity,
  SpatiumGitIntegration,
  SpatiumTime,
} from '../models/spatium-config.interface';

import { SpatiumDatabase } from '../spatium-database';
import { BaseRepositoryV2 } from './infrastructure/base-repository-v2';

@singleton()
export class ConfigRepository extends BaseRepositoryV2<SpatiumConfigBaseEntity<any, any>> {
  private overrides: [string, any][];

  public get git() {
    return this.get('git').then(
      g =>
        ({
          enabled: true,
          email: 'example@example.com',
          name: 'system',
          privateKeyPath: resolve(process.env.HOME, '.ssh/id_rsa'),
          publicKeyPath: resolve(process.env.HOME, '.ssh/id_rsa.pub'),
          ...g,
        } as SpatiumGitIntegration)
    );
  }

  public get dropbox() {
    return this.get('dropbox');
  }

  public get time() {
    return this.get('time').then(
      t =>
        t ??
        ({
          ianaZone: 'Europe/Copenhagen',
          locale: 'da',
        } as SpatiumTime)
    );
  }

  public get cvrApi() {
    return this.get('cvrApi');
  }

  public get lastSelection() {
    return this.get('lastSelection');
  }

  constructor(public database: SpatiumDatabase) {
    super(database.config);
  }

  public async get<TKey extends keyof SpatiumConfig>(key: TKey): Promise<SpatiumConfig[TKey]> {
    if (!this.overrides) {
      await this.setOverrides();
    }

    const result: SpatiumConfigBaseEntity<TKey, SpatiumConfig[TKey]>[] = await this.where('key', key);
    const values = result.map(item => item.value);
    const value = values && values.length ? values[0] : null;
    const valueWithOverrides = this.applyOverrides(key, value);

    return valueWithOverrides;
  }

  public async set<TKey extends keyof SpatiumConfig>(key: TKey, value: Partial<SpatiumConfig[TKey]>) {
    const existing = await this.findEntity(key);

    if (existing === null) {
      await this.create({ key, value });
    } else {
      existing.value = {
        ...existing.value,
        ...value,
      };

      await this.update(existing.id, existing);
    }
  }

  private applyOverrides<TKey extends keyof SpatiumConfig>(baseKey: TKey, value: SpatiumConfig[TKey]) {
    if (this.overrides.length === 0 || this.overrides.filter(([path]) => path.startsWith(baseKey)).length === 0) {
      this.log('Did not find config overrides');

      return value;
    }

    const relevantOverrides = this.overrides.filter(([path]) => path.startsWith(baseKey));

    this.log(`Found ${relevantOverrides.length} config overrides`);

    for (const [path, overrideValue] of relevantOverrides) {
      const trimmedPath = path.replace(`${baseKey}.`, '');
      this.log(`Overriding: '${path}' with '${overrideValue}'`);

      setPath(value, trimmedPath, overrideValue);
    }

    return value;
  }

  private async setOverrides() {
    const overridesPath = resolve(process.cwd(), '.spatiumoverrides');

    this.overrides = [];

    if (!existsSync(overridesPath)) {
      return;
    }

    const lines = createInterface({
      input: createReadStream(overridesPath),
      crlfDelay: Infinity,
    });

    for await (const line of lines) {
      if (!line) {
        continue;
      }

      this.overrides.push(this.parseOverrideLine(line));
    }
  }

  private parseOverrideLine(line: string): [string, any] {
    const split = line.split('=');

    if (split.length === 1) {
      return [split[0], ''];
    } else if (split.length === 2) {
      return [split[0], split[1]];
    } else {
      throw new Error(`invalid config override: '${line}'`);
    }
  }

  private async findEntity<TKey extends keyof SpatiumConfig>(
    key: TKey
  ): Promise<SpatiumConfigBaseEntity<TKey, SpatiumConfig[TKey]>> {
    const result: SpatiumConfigBaseEntity<TKey, SpatiumConfig[TKey]>[] = await this.where('key', key);
    const value = result && result.length ? result[0] : null;

    return value;
  }

  private log(message: string) {
    info(`[config-repository] ${message}`);
  }
}
