import { singleton } from 'tsyringe';
import { User } from '../models/user.interface';

import { SpatiumDatabase } from '../spatium-database';
import { BaseRepositoryV2 } from './infrastructure/base-repository-v2';

@singleton()
export class UserRepository extends BaseRepositoryV2<User> {
  constructor(public database: SpatiumDatabase) {
    super(database.users);
  }
}
