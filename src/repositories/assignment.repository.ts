import { singleton } from 'tsyringe';
import { Assignment } from '../models/assignment.interface';

import { SpatiumDatabase } from '../spatium-database';
import { AgentRepository } from './agent.repository';
import { CustomerRepository } from './customer.repository';
import { BaseRepositoryV2 } from './infrastructure/base-repository-v2';
import { UserRepository } from './user.repository';

@singleton()
export class AssignmentRepository extends BaseRepositoryV2<Assignment> {
  constructor(
    public database: SpatiumDatabase,
    public users: UserRepository,
    public agents: AgentRepository,
    public customers: CustomerRepository
  ) {
    super(database.assignments);
  }
}
