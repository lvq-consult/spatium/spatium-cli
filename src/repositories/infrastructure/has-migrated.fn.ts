import { existsSync } from 'fs-extra';
import { resolve } from 'path';
import { getDataPath } from './base.repository';

export function pathExists(filepath: string) {
  return new Promise<boolean>(accept => accept(existsSync(filepath)));
}

export function hasMigrated(name: string) {
  return pathExists(resolve(getDataPath(), 'v2', name));
}
