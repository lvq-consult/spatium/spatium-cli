import { createFile, existsSync, readJson, writeJson } from 'fs-extra';
import { basename, resolve } from 'path';
import { v4 as uuidv4 } from 'uuid';
import { info, warn } from '../../logger';
import { BaseModel } from '../../models/base.interface';
import { getDataPath, IBaseRepository } from './base.repository';
import { Predicate } from './predicate.type';

interface DataFile<T extends BaseModel> {
  entries: [string, T][];
}

export class LegacyBaseRepositoryHandler<T extends BaseModel> implements IBaseRepository<T> {
  public ready = this.init();
  public entries = new Map<string, T>();

  constructor(public name: string) {}

  public whereAsync<K extends keyof T>(key: K, value: T[K]): Promise<T[]> {
    return Promise.resolve(this.where(key, value));
  }
  public pluckAsync<K extends keyof T>(key: K): Promise<T[K][]> {
    return Promise.resolve(this.pluck(key));
  }
  public anyAsync(predicate: (item: T) => boolean): Promise<boolean> {
    return Promise.resolve(this.any(predicate));
  }
  public noneAsync(predicate: (item: T) => boolean): Promise<boolean> {
    return Promise.resolve(this.none(predicate));
  }
  public allAsync(): Promise<T[]> {
    return Promise.resolve(this.all());
  }
  public findAsync(predicate: (item: T) => boolean): Promise<T[]> {
    return Promise.resolve(this.find(predicate));
  }
  public findOneAsync(predicate: string | Predicate<T>): Promise<T> {
    return Promise.resolve(this.findOne(predicate));
  }

  public where<K extends keyof T>(key: K, value: T[K]): T[] {
    return this.all().filter(item => item[key] === value);
  }

  public pluck<K extends keyof T>(key: K): T[K][] {
    return this.all().map(item => item[key]);
  }

  public any(predicate: (item: T) => boolean) {
    return this.find(item => predicate(item)).length > 0;
  }

  public none(predicate: (item: T) => boolean) {
    return this.find(item => predicate(item)).length === 0;
  }

  public all() {
    return this.collectEntries().map(([id, entry]) => entry);
  }

  public find(predicate: (item: T) => boolean) {
    const collection: T[] = [];
    const iter = this.entries.entries();
    let current = iter.next();

    while (current.done !== true) {
      const { value } = current;
      const [id, entry] = value;

      if (predicate(entry)) {
        collection.push(entry);
      }

      current = iter.next();
    }

    return collection;
  }

  public findOne(predicate: string | Predicate<T>) {
    if (typeof predicate === 'string') {
      const entry = this.entries.get(predicate);

      return entry;
    } else {
      const entries = this.find(i => predicate(i));

      return entries.length > 0 ? entries[0] : undefined;
    }
  }

  public async create(entry: T) {
    const id = uuidv4();
    const data = {
      ...entry,
      id,
    } as T;
    this.entries.set(id, data);

    await this.commit();

    return entry;
  }

  public async update(id: string, patch: Partial<T>) {
    const entry = this.findOne(id);
    const data = {
      ...entry,
      ...patch,
    } as T;

    this.entries.set(id, data);

    await this.commit();

    return data;
  }

  public async delete(id: string) {
    this.entries.delete(id);
    await this.commit();
  }

  private async commit() {
    const dataFilePath = this.getDataFilePath();
    const dataFile: DataFile<T> = {
      entries: this.collectEntries(),
    };
    info('writing to', dataFilePath);
    await writeJson(dataFilePath, dataFile, { encoding: 'utf-8', spaces: 2 });
  }

  private collectEntries() {
    const collection: [string, T][] = [];
    const iter = this.entries.entries();
    let current = iter.next();

    while (current.done !== true) {
      collection.push(current.value);
      current = iter.next();
    }

    return collection;
  }

  protected async init() {
    const dataFilePath = this.getDataFilePath();
    const dataFileExists = await this.pathExists(dataFilePath);

    if (!dataFileExists) {
      info(basename(dataFilePath), 'does not exists creating empty file.');
      await createFile(dataFilePath);
      await writeJson(dataFilePath, { entries: [] }, { encoding: 'utf-8' });
    } else {
      info(basename(dataFilePath), 'already exists, loading data.');

      let dataFile: DataFile<T>;

      try {
        dataFile = await readJson(dataFilePath, { encoding: 'utf-8' });
      } catch (e) {
        warn(`error while reading data file ${dataFilePath}`, e);
      }

      if (dataFile && !dataFile.entries) {
        warn(`${basename(dataFilePath)} invalid format.`);
      }

      const entries = !dataFile || !dataFile.entries ? [] : dataFile.entries;

      this.entries = new Map<string, T>(entries);
    }
  }

  private getDataFilePath() {
    const filename = this.getFilename();
    const dataPath = getDataPath();

    return resolve(dataPath, filename);
  }

  private getFilename() {
    return `${this.name}.entries.json`;
  }

  private pathExists(filepath: string) {
    return new Promise(accept => accept(existsSync(filepath)));
  }
}
