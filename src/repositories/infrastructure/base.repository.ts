import { resolve } from 'path';
import { info } from '../../logger';
import { BaseModel } from '../../models/base.interface';

import { LegacyBaseRepositoryHandler } from './legacy-base-repository-handler';
import { Predicate } from './predicate.type';

export function getDataPath() {
  const basePath = process.cwd();
  const dataPath = 'data';

  return resolve(basePath, dataPath);
}

export interface IBaseRepository<T extends BaseModel> {
  name: string;
  ready: Promise<void>;
  entries: Map<string, T>;
  where<K extends keyof T>(key: K, value: T[K]): T[];
  pluck<K extends keyof T>(key: K): T[K][];
  any(predicate: (item: T) => boolean): boolean;
  none(predicate: (item: T) => boolean): boolean;
  all(): T[];
  find(predicate: (item: T) => boolean): T[];
  findOne(predicate: string | Predicate<T>): T;
  create(entry: T): Promise<T>;
  update(id: string, patch: Partial<T>): Promise<T>;
  delete(id: string): Promise<void>;
}

export abstract class BaseRepository<T extends BaseModel> implements IBaseRepository<T> {
  private handler: IBaseRepository<T>;

  public ready = this.init();
  public get entries() {
    if (this.handler) {
      return this.handler.entries;
    } else {
      return new Map<string, T>();
    }
  }

  constructor(public name: string) {}

  public async setDefaultValues() {}

  public where<K extends keyof T>(key: K, value: T[K]): T[] {
    return this.handler.where(key, value);
  }

  public pluck<K extends keyof T>(key: K): T[K][] {
    return this.handler.pluck(key);
  }

  public any(predicate: (item: T) => boolean) {
    return this.handler.any(predicate);
  }

  public none(predicate: (item: T) => boolean) {
    return this.handler.none(predicate);
  }

  public all() {
    return this.handler.all();
  }

  public find(predicate: (item: T) => boolean) {
    return this.handler.find(predicate);
  }

  public findOne(predicate: string | Predicate<T>) {
    return this.handler.findOne(predicate);
  }

  public async create(entry: T) {
    const data = await this.handler.create(entry);

    return data;
  }

  public async update(id: string, patch: Partial<T>) {
    const data = await this.handler.update(id, patch);

    return data;
  }

  public async save(entry: T) {
    if (entry.id) {
      return await this.update(entry.id, entry);
    } else {
      return await this.create(entry);
    }
  }

  public async delete(id: string) {
    const copy = { ...this.findOne(id) };
    await this.handler.delete(id);
  }

  protected async init() {
    info(`'${this.name}'-repository ready`);

    this.handler = new LegacyBaseRepositoryHandler<T>(this.name);

    await this.handler.ready;
  }
}
