import { remove } from 'fs-extra';
import { resolve } from 'path';
import { container } from 'tsyringe';
import { info } from '../../logger';
import { BaseModel } from '../../models/base.interface';
import { SpatiumDatabase } from '../../spatium-database';
import { getDataPath } from './base.repository';
import { hasMigrated } from './has-migrated.fn';
import { LegacyBaseRepositoryHandler } from './legacy-base-repository-handler';

export class Migrater<T extends BaseModel> {
  private get oldDataFile() {
    const filename = `${this.handler.name}.entries.json`;
    const dataPath = getDataPath();

    return resolve(dataPath, filename);
  }

  private table = container.resolve(SpatiumDatabase).getTable(this.handler.name);

  constructor(private handler: LegacyBaseRepositoryHandler<T>) {}

  public hasMigrated() {
    return hasMigrated(this.handler.name);
  }

  public async migrate(progress: () => void, removeOldData = false) {
    this.log(`Start migration`);
    await this.table.ensureExists();
    await this.migrateEntries(() => progress());

    if (removeOldData) {
      await this.removeOldData();
    }

    this.log(`Migration finished`);
  }

  private async migrateEntries(progress: () => void) {
    const entries = this.handler.all();

    this.log(`Migrating ${entries.length} entries`);

    for (const entry of entries) {
      await this.migrateEntry(entry);

      progress();
    }
  }

  private async migrateEntry(entry: T) {
    this.log(`Migrating entry ${entry.id}`);

    try {
      await this.table.create(entry);
    } catch (e) {
      console.error(e);
    }
  }

  private async removeOldData() {
    this.log(`Removing old data file: ${this.oldDataFile}`);

    await remove(this.oldDataFile);
  }

  private log(message: string) {
    info(`[migrater(${this.handler.name})] ${message}`);
  }
}
