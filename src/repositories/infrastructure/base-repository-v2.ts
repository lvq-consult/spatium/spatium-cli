import { SpatiumEntity, SpatiumTable } from '@lvqconsult/spatium-db';
import { Predicate } from './predicate.type';

export abstract class BaseRepositoryV2<T extends SpatiumEntity> {
  constructor(protected table: SpatiumTable<T>) {}

  public search<TProperty extends keyof T>(searchValue: T[TProperty], ...properties: TProperty[]) {
    return this.table.search(searchValue, ...properties);
  }

  public searchStrictly<TProperty extends keyof T>(searchValue: T[TProperty], ...properties: TProperty[]) {
    return this.table.searchStrictly(searchValue, ...properties);
  }

  public async searchWhere<TProperty extends keyof T>(
    whereProperty: TProperty,
    whereValue: T[TProperty],
    searchValue: T[TProperty],
    ...properties: TProperty[]
  ) {
    return this.table.searchWhere(whereProperty, whereValue, searchValue, ...properties);
  }

  public async where<K extends keyof T>(key: K, value: T[K]): Promise<T[]> {
    return await this.table.where(key, value);
  }

  public async pluck<K extends keyof T>(key: K): Promise<T[K][]> {
    return await this.table.pluck(key);
  }

  public async pluckWhere<TKey extends keyof T, TSearchKey extends keyof T>(
    key: TKey,
    searchKey: TSearchKey,
    value: T[TSearchKey]
  ) {
    return this.table.pluckWhere(key, searchKey, value);
  }

  public async any(predicate: (item: T) => boolean): Promise<boolean> {
    return await this.table.any(item => predicate(item));
  }

  public async none(predicate: (item: T) => boolean): Promise<boolean> {
    return await this.table.none(item => predicate(item));
  }

  public async all(): Promise<T[]> {
    return await this.table.collectAll();
  }

  public async find(predicate: (item: T) => boolean): Promise<T[]> {
    return await this.table.find(item => predicate(item));
  }

  public async findOne(predicate: string | Predicate<T>): Promise<T> {
    if (predicate === undefined || predicate === null) {
      throw new Error('Predicate was undefined or null');
    }

    if (typeof predicate === 'string') {
      return await this.table.get(predicate);
    } else if (typeof predicate === 'function') {
      const results = await this.table.find(item => predicate(item));

      return results.length > 0 ? results[0] : null;
    } else {
      throw new Error(`Predicate can not be of type '${typeof predicate}'`);
    }
  }

  public async create(entry: T): Promise<T> {
    const result = await this.table.create(entry);

    return result;
  }

  public async update(id: string, patch: Partial<T>): Promise<T> {
    const result = await this.table.update(id, patch);

    return result;
  }

  public async delete(id: string): Promise<void> {
    await this.table.delete(id);
  }

  public async save(entry: T) {
    if (entry.id) {
      return await this.create(entry);
    } else {
      return await this.update(entry.id, entry);
    }
  }
}
