import { singleton } from 'tsyringe';
import { Timesheet } from '../models/timesheet.interface';

import { DateHelperService } from '../services/date-helper.service';
import { SpatiumDatabase } from '../spatium-database';
import { CustomerRepository } from './customer.repository';
import { BaseRepositoryV2 } from './infrastructure/base-repository-v2';
import { UserRepository } from './user.repository';

@singleton()
export class TimesheetRepository extends BaseRepositoryV2<Timesheet> {
  constructor(
    public database: SpatiumDatabase,
    public users: UserRepository,
    public customers: CustomerRepository,
    public dateHelper: DateHelperService
  ) {
    super(database.timesheets);
  }

  public async latestTask(customerId: string) {
    const timesheets = await this.searchStrictly(customerId, 'customerId');

    if (timesheets.length === 0) {
      return undefined;
    }

    return timesheets.pop();
  }
}
