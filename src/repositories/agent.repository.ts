import { singleton } from 'tsyringe';
import { Agent } from '../models/agent.interface';

import { SpatiumDatabase } from '../spatium-database';
import { BaseRepositoryV2 } from './infrastructure/base-repository-v2';

@singleton()
export class AgentRepository extends BaseRepositoryV2<Agent> {
  constructor(public database: SpatiumDatabase) {
    super(database.agents);
  }
}
