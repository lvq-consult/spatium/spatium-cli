import { singleton } from 'tsyringe';

@singleton()
export class LogService {
  private silent = true;

  public unsilence() {
    this.silent = false;
  }

  public silence() {
    this.silent = true;
  }

  public timestamp() {
    return `[${new Date().toISOString()}]`;
  }

  public log(...args: any[]) {
    if (this.silent) {
      return;
    }

    console.log(this.timestamp(), ...args);
  }

  public warn(...args: any[]) {
    if (this.silent) {
      return;
    }

    console.warn(this.timestamp(), ...args);
  }

  public info(...args: any[]) {
    if (this.silent) {
      return;
    }

    // tslint:disable-next-line:no-console
    console.info(this.timestamp(), ...args);
  }

  public error(...args: any[]) {
    if (this.silent) {
      return;
    }

    console.error(this.timestamp(), ...args);
  }
}
