import { DateTime, Duration, Zone } from 'luxon';
import { singleton } from 'tsyringe';
import { ConfigRepository } from '../repositories/config.repository';

@singleton()
export class DateHelperService {
  private locale: string;
  private zone: string | Zone;

  constructor(private config: ConfigRepository) {}

  public async ensureSyncDefaults() {
    const time = await this.config.time;

    this.locale = time.locale;
    this.zone = time.ianaZone;
  }

  public async fromISO(input: string) {
    const time = await this.config.time;

    return DateTime.fromISO(input, {
      locale: time.locale,
      zone: time.ianaZone,
      setZone: true,
    })
      .setLocale(time.locale)
      .setZone(time.ianaZone);
  }

  public async local() {
    const time = await this.config.time;

    return DateTime.local().setLocale(time.locale).setZone(time.ianaZone);
  }

  public async yesterday() {
    return (await this.local()).minus(Duration.fromObject({ days: 1 }));
  }

  public async yesterdayISO() {
    return (await this.yesterday()).toISODate();
  }

  public async today() {
    return this.local();
  }

  public async todayISO() {
    return (await this.local()).toISODate();
  }

  public async startOfMonth() {
    return (await this.local()).startOf('month').toISO();
  }

  public async startOfMonthDate() {
    return (await this.local()).startOf('month').toJSDate();
  }

  public async endOfMonth() {
    return (await this.local()).endOf('month').toISO();
  }

  public async endOfMonthDate() {
    return (await this.local()).endOf('month').toJSDate();
  }

  public async startOfMonthDateTime() {
    return (await this.local()).startOf('month');
  }

  public async endOfMonthDateTime() {
    return (await this.local()).endOf('month');
  }

  public async formatDate(isoString: string) {
    return (await this.fromISO(isoString)).toISODate();
  }

  public async formatDateIndexFromIso(isoString: string) {
    const startDate = await this.fromISO(isoString);
    const month = startDate.month;
    const year = startDate.year;

    return `${year}-${month}`;
  }

  public async formatDateIndex(startDate: DateTime) {
    const month = startDate.month.toString();
    const year = startDate.year;

    return `${year}-${month.length === 1 ? `0${month}` : month}`;
  }

  public fromISOSync(input: string) {
    return DateTime.fromISO(input, {
      locale: this.locale,
      zone: this.zone,
      setZone: true,
    })
      .setLocale(this.locale)
      .setZone(this.zone);
  }

  public formatDateSync(isoString: string) {
    return this.fromISOSync(isoString).toISODate();
  }

  public formatToISOSync(input: string) {
    return this.fromISOSync(input).toISO();
  }

  public isValidDateSync(input: string) {
    return this.fromISOSync(input).isValid;
  }
}
