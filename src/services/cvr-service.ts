import axios from 'axios';
import { singleton } from 'tsyringe';
import { ConfigRepository } from '../repositories/config.repository';
import { LogService } from './log.service';

export enum CvrSearchOptions {
  Regular = 'search',
  Vat = 'vat',
  Name = 'name',
  ProductionUnit = 'produ',
  Phone = 'phone',
}

export enum CvrSearchCountries {
  Denmark = 'dk',
  Norway = 'no',
}

export interface ApiOwners {
  name: string;
}

export interface ApipPoductionunits {
  pno: string;
  name: string;
  address: string;
  zipcode: string;
  city: string;
  protected: boolean;
  phone: string;
  email: string;
  fax: string;
  startdate: string;
  enddate: string;
  employees: string;
  addressco: string;
  industrycode: number;
  industrydesc: string;
  companycode: number;
  companydesc: string;
  creditstartdate: string;
  creditstatus: number;
  creditbankrupt: boolean;
}

export interface ApiResult {
  vat: string;
  name: string;
  address: string;
  zipcode: string;
  city: string;
  protected: boolean;
  phone: string;
  email: string;
  fax: string;
  startdate: string;
  enddate: string;
  employees: string;
  addressco: string;
  industrycode: number;
  industrydesc: string;
  companycode: number;
  companydesc: string;
  creditstartdate: string;
  creditstatus?: number;
  creditbankrupt: boolean;
  owners: ApiOwners[];
  productionunits: ApipPoductionunits[];
  t: number;
  version: number;
}

@singleton()
export class CvrService {
  constructor(public config: ConfigRepository, public logService: LogService) {}

  public async search(
    searchString: string,
    searchOption: CvrSearchOptions = CvrSearchOptions.Regular,
    country: CvrSearchCountries = CvrSearchCountries.Denmark
  ) {
    const cvrApiConfig = await this.config.cvrApi;

    if (!cvrApiConfig || cvrApiConfig?.enabled === false) {
      throw new Error(`Run command 'integrate:cvrapi config' in order to configure CVR API.`);
    }

    const { companyName, projectName, contactName, contactEmail, contactPhone, useEmail } = cvrApiConfig;
    const url = `https://cvrapi.dk/api?country=${country}&${searchOption}=${searchString}`;
    this.logService.info(`[cvr-service] Looking up: ${url}`);
    const userAgent = `${companyName} - ${projectName} - ${contactName} ${useEmail ? contactEmail : contactPhone}`;
    this.logService.info(`[cvr-service] Using User-Agent: ${userAgent}`);
    const response = await axios.get<ApiResult>(url, {
      headers: {
        'User-Agent': userAgent,
      },
    });

    if (response?.status !== 200 || !response?.data || !response?.data?.name || !response?.data?.vat) {
      this.logService.error(
        `[cvr-service] Unable to look up company.\nStatus=${response?.status}\nStatusText=${
          response?.statusText
        }\nData=\n${JSON.stringify(response?.data, null, 2)}\nHeaders=\n${JSON.stringify(response?.headers, null, 2)}`
      );

      return null;
    }

    return {
      name: response?.data?.name,
      cvr: response?.data?.vat,
    };
  }
}
