import { container } from 'tsyringe';
import { LogService } from './services/log.service';

const logService = container.resolve(LogService);

export function unsilence() {
  logService.unsilence();
}

export function silence() {
  logService.silence();
}

export function timestamp() {
  return logService.timestamp();
}

export function log(...args: any[]) {
  logService.log(...args);
}

export function warn(...args: any[]) {
  logService.warn(...args);
}

export function info(...args: any[]) {
  logService.info(...args);
}

export function error(...args: any[]) {
  logService.error(...args);
}
