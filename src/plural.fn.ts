export function plural(
  count: number,
  baseWord: string,
  pluralSuffix: string,
  singularSuffix = '',
  singularRange: number[] = [1]
) {
  if (singularRange.some(n => n === count)) {
    return `${baseWord}${singularSuffix}`;
  } else {
    return `${baseWord}${pluralSuffix}`;
  }
}
