import { plural } from '../src/plural.fn';

test('should not add suffix if count is in singular range', () => {
  // Arrange
  const expectedValue = 'test';

  // Act
  const actualValue = plural(1, 'test', 's');

  // Assert
  expect(actualValue).toBe(expectedValue);
});

test('should add suffix if count is not in singular range', () => {
  // Arrange
  const expectedValue = 'tests';

  // Act
  const actualValue = plural(2, 'test', 's');

  // Assert
  expect(actualValue).toBe(expectedValue);
});
